//
//  CompleteViewController.h
//  Naptimer
//
//  Created by 大口 尚紀(管理者) on 2014/06/06.
//  Copyright (c) 2014年 大口 尚紀(管理者). All rights reserved.
//

#import <UIKit/UIKit.h>
#include <AVFoundation/AVFoundation.h>

@interface CompleteViewController : UIViewController

@property (weak, nonatomic) IBOutlet UINavigationItem *compNav;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property  AVAudioPlayer *player;
- (IBAction)goTop;
@end
