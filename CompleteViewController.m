//
//  CompleteViewController.m
//  Naptimer
//
//  Created by 大口 尚紀(管理者) on 2014/06/06.
//  Copyright (c) 2014年 大口 尚紀(管理者). All rights reserved.
//

#import "CompleteViewController.h"
#import "MasterViewController.h"
#import <AudioToolbox/AudioToolbox.h>
#import "AppDelegate.h"


#define UIColorHex(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

@interface CompleteViewController ()

@end

@implementation CompleteViewController{
    NSTimer *tm;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    CGRect rect1 = [[UIScreen mainScreen] bounds];
    id appDelegate = (id)[[UIApplication sharedApplication] delegate];
    self.managedObjectContext = [appDelegate managedObjectContext];

    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
        UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectZero];
    titleLabel.font = [UIFont fontWithName:@"Helvetica" size:15];
    titleLabel.text = @"";
    titleLabel.textColor = [UIColor grayColor];
    [titleLabel sizeToFit];
    self.navigationItem.titleView = titleLabel;
    
    UIView *chart = [[UIView alloc] initWithFrame:CGRectMake(60, 100, 200, 200)];
    chart.center = CGPointMake(rect1.size.width/2, chart.center.y);
    chart.backgroundColor = [UIColor grayColor];
    chart.layer.cornerRadius = 100;
    [self.view addSubview:chart];
    UIBezierPath *path = path = [UIBezierPath bezierPath];
    [path moveToPoint:CGPointMake(100, 100)];
    [path addArcWithCenter:CGPointMake(100, 100) radius:100 startAngle:-0.5*M_PI endAngle:-0.5*M_PI + ((2*M_PI)) clockwise:YES];
    CAShapeLayer *sl;
    sl = [[CAShapeLayer alloc] init];
    sl.fillColor = [self color:1].CGColor;
    sl.path = path.CGPath;
    [chart.layer addSublayer:sl];

    UILabel *percentLabel = [[UILabel alloc] init];
    percentLabel.frame = CGRectMake(102, chart.center.y-25, 120, 50);
    percentLabel.font = [UIFont fontWithName:@"Helvetica" size:23];
    percentLabel.textColor = [UIColor grayColor];
    percentLabel.textAlignment = NSTextAlignmentCenter;
    percentLabel.text = @"100.00%";
    percentLabel.center = CGPointMake(rect1.size.width/2 + 2, percentLabel.center.y);
    [self.view addSubview:percentLabel];
    
    UIView *mask = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 120, 120)];
    mask.layer.cornerRadius = 60;
    mask.center = CGPointMake(100, 100);
    mask.backgroundColor = [UIColor whiteColor];
    [chart addSubview:mask];
    
    UILabel *meterLabel = [[UILabel alloc] init];
    meterLabel.frame = CGRectMake(102, 290, 120, 50);
    meterLabel.font = [UIFont fontWithName:@"Helvetica" size:20];
    meterLabel.textColor = [UIColor grayColor];
    meterLabel.textAlignment = NSTextAlignmentCenter;
    meterLabel.text = @"move meter";
    meterLabel.center = CGPointMake(rect1.size.width/2 + 2, meterLabel.center.y);
    [self.view addSubview:meterLabel];

    [self playSound];
    
    UILabel *completeLabel = [[UILabel alloc] init];
    completeLabel.frame = CGRectMake(0, rect1.size.height-(rect1.size.height/1.17), rect1.size.width, 500);
    completeLabel.font = [UIFont fontWithName:@"Helvetica" size:40];
    completeLabel.textAlignment = NSTextAlignmentCenter;
    completeLabel.textColor = [UIColor grayColor];
    completeLabel.numberOfLines = 0;
    completeLabel.text = @"Complete !!";
    
    UILabel *compSubLabel = [[UILabel alloc] init];
    compSubLabel.frame = CGRectMake(0, rect1.size.height-(rect1.size.height/1.3), rect1.size.width, 500);
    compSubLabel.font = [UIFont fontWithName:@"Helvetica" size:17];
    compSubLabel.textColor = [UIColor grayColor];
    compSubLabel.textAlignment = NSTextAlignmentCenter;
    compSubLabel.numberOfLines = 0;
    compSubLabel.text = NSLocalizedString(@"goodmor",nil);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    [UIView beginAnimations:nil context:context];
    //アニメーションを実行する時間
    [UIView setAnimationDuration:3.0];
    completeLabel.alpha = 0;
    compSubLabel.alpha = 0;
    //アニメーションイベントを受け取るview
    [UIView setAnimationDelegate:self.view];
    
    [self.view addSubview:completeLabel];
    [self.view addSubview:compSubLabel];
    [completeLabel setAlpha:1.0];
    [compSubLabel setAlpha:1.0];
    [UIView commitAnimations];
    tm = [NSTimer scheduledTimerWithTimeInterval: 30 target:self selector:@selector(goTop) userInfo:nil repeats:NO];
  
    AppDelegate *appDelegete = [[UIApplication sharedApplication] delegate];
    //    self.view.backgroundColor = [DetailViewController color:4];
    appDelegete.itemArray = [NSMutableArray array];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *my_entity
    = [NSEntityDescription entityForName:@"Myschedule" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:my_entity];
    
    // ソート条件を指定します。
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"createdat" ascending:NO];
    NSArray *my_sortDescriptors = @[sortDescriptor];
    [fetchRequest setSortDescriptors:my_sortDescriptors];
    NSArray *my_result= [self.managedObjectContext executeFetchRequest:fetchRequest error:nil];
    [appDelegete.itemArray removeAllObjects];
    for (int i = 0; i < my_result.count; i++) {
        id obj = [my_result objectAtIndex: i];
        [appDelegete.itemArray addObject:[obj valueForKey:@"minute"]];
    }
    
//   [self presentNotificationViewController];
//    AppDelegate *appDelegete = [[UIApplication sharedApplication] delegate];
//    appDelegete.on_alarm = false;
    [NSTimer scheduledTimerWithTimeInterval: 1.5 target:self selector:@selector(goAlert) userInfo:nil repeats:NO];
}

-(void)viewWillAppear
{
//    MasterViewController *newVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MasterView"];
//    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:newVC];
//    [self.navigationController presentViewController:nav animated:NO completion:nil];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (UIColor*)color:(int)i
{
    switch (i) {
        case 0:
            return UIColorHex(0xF24495);
        case 1:
            return UIColorHex(0x04BFBF);
        case 2:
            return UIColorHex(0xB2F252);
        case 3:
            return UIColorHex(0xF2CB05);
        case 4:
            return UIColorHex(0xE9F2DF);
        default:
            break;
    }
    return nil;
}

- (void)playSound{
    NSString *soundPath = [[NSBundle mainBundle] pathForResource:@"complete" ofType:@"mp3"];
    NSURL *url = [NSURL fileURLWithPath:soundPath];
    self.player = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:NULL];
    [self.player setNumberOfLoops:0];
    self.player.volume = 0.1;
    self.player.numberOfLoops = 0;
    self.player.delegate = (id)self;
    [self.player prepareToPlay];
    [self.player play];
}

- (void)goAlert{
     [self performSegueWithIdentifier:@"goAlertSegue" sender:self];
}
- (void)goTop{
    if ([tm isValid]) {
        [tm invalidate];
    }
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    id firstViewController = [storyboard instantiateInitialViewController];
    AppDelegate *appDelegete = [[UIApplication sharedApplication] delegate];
    appDelegete.window.rootViewController = firstViewController;
    [appDelegete.window makeKeyAndVisible];
}

- (void)prefSegue{
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    UITabBarController *tbc = (UITabBarController *)window.rootViewController;
    UINavigationController *navigationController = tbc.viewControllers[0];
    
    NSString* viewIdentifier = @"CompleteView";
    UIStoryboard* sb = self.storyboard;
    UIViewController* vc = [[UIViewController alloc] init];
    vc = [sb instantiateViewControllerWithIdentifier:viewIdentifier];
//    [navigationController dismissViewControllerAnimated:YES completion:nil];
    [navigationController popToViewController:vc animated:YES];
}

//    UITabBarController *tabBarController = (UITabBarController *)[[[UIApplication sharedApplication] delegate] window].rootViewController;
//    UINavigationController *navController = [[tabBarController viewControllers] objectAtIndex:0];
//    [navController popToRootViewControllerAnimated:NO];}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
