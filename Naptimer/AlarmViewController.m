//
//  AlarmViewController.m
//  Naptimer
//
//  Created by 大口 尚紀(管理者) on 2014/07/15.
//  Copyright (c) 2014年 大口 尚紀(管理者). All rights reserved.
//

#import "AlarmViewController.h"
#import "AlarmsetupViewController.h"
#import "CompleteViewController.h"
#import "AppDelegate.h"

@interface AlarmViewController ()
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;
@end

@implementation AlarmViewController{
    NSMutableArray *itemArray;
    NSIndexPath *selected_index;
    bool edit_mode;
    NSDate *minDate;
    CGRect tabBar;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tableView.contentInset = UIEdgeInsetsMake(0.0f, 0.0f, 100.0f, 0.0f);
    id appDelegate = (id)[[UIApplication sharedApplication] delegate];
    self.managedObjectContext = [appDelegate managedObjectContext];
    tabBar = self.tabBarController.tabBar.frame;
    //  ナビゲーションバーのタイトルの色を白色に変更する
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectZero];
    titleLabel.font = [UIFont fontWithName:@"Helvetica" size:15];
    titleLabel.text = NSLocalizedString(@"alarm",nil);
    titleLabel.textColor = [UIColor grayColor];
    [titleLabel sizeToFit];
    self.navigationItem.titleView = titleLabel;
    
    if([[self.fetchedResultsController fetchedObjects] count] > 0){
        //	ナビゲーションバーの左ボタンを「編集」に変更。
	    //	押された時に呼び出すメソッドもenterEditModeにする。
    	//	テーブルビューを編集モードから離れさせる。
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"edit",nil) style:UIBarButtonItemStyleBordered target:self action:@selector(enterEditMode:)];
    }
    //	テーブルビューを編集モードから離れさせる。
    [self.tableView endUpdates];
    [self.tableView setEditing:NO animated:YES];
    [self.tableView setAllowsSelectionDuringEditing:YES];
    self.tableView.allowsSelection = NO;
    //    self.view.backgroundColor = [DetailViewController color:4];
    itemArray = [NSMutableArray array];
    selected_index = 0;
    edit_mode = NO;
    
    int countOn = 0;
    for (int i = 0; i < [[_fetchedResultsController fetchedObjects] count]; i++) {
        NSNumber *on = [[[_fetchedResultsController fetchedObjects] valueForKey:@"on"] objectAtIndex:i];
        if([on intValue] == 1){
            countOn++;
        }
    }
    
    if(countOn > 2){
        UIAlertView *alert = [[UIAlertView alloc]
                              initWithTitle:NSLocalizedString(@"AlarmNumTit",nil)
                              message:NSLocalizedString(@"AlarmNumMes",nil)
                              delegate:self
                              cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
        alert.tag = 0;
        [alert show];
    }
}

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    UITabBarController *tabBarController = self.tabBarController;
    GADBannerView *bannerView = (GADBannerView*)[tabBarController.view viewWithTag:9999999];
    if(bannerView != nil){
        CGRect bannerFrame = bannerView.frame;
        bannerFrame.origin.y = tabBarController.view.frame.size.height - bannerView.frame.size.height - 49;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            CGSize screenSize = [[UIScreen mainScreen] bounds].size;
            if (@available(iOS 11.0, *)) {
                UIWindow *window = UIApplication.sharedApplication.keyWindow;
                CGFloat bottomPadding = window.safeAreaInsets.bottom;
                bannerFrame.origin.y = tabBarController.view.frame.size.height - bannerView.frame.size.height - bottomPadding - 1;
            }
        }
        bannerView.frame = bannerFrame;
    }

    
    switch (alertView.tag) {
        case 0: {
            for (int i = 0; i < [[_fetchedResultsController fetchedObjects] count]; i++) {
                NSManagedObject *updObject = [_fetchedResultsController.fetchedObjects objectAtIndex:i];
                [updObject setValue:[NSNumber numberWithBool:NO] forKey:@"on"];
            }
            // saveメソッドでデータをDBに保存します。
            // エラー発生時には、引数に参照渡しで渡しているErrorオブジェクトの中身を表示します。
            NSError *error = nil;
            if (![self.managedObjectContext save:&error]) {
                NSLog(@"error = %@", error);
                
            } else {
                NSLog(@"Insert Completed.");
            }
            break;
            
        }
        case 1:{
            for (int i = 0; i < [[_fetchedResultsController fetchedObjects] count]; i++) {
                NSManagedObject *updObject = [_fetchedResultsController.fetchedObjects objectAtIndex:i];
                NSDate *createdat = [[[self.fetchedResultsController  fetchedObjects] valueForKey:@"createdat"] objectAtIndex:i];
                if(minDate != nil){
                    NSComparisonResult result = [createdat compare:minDate];
                    switch (result) {
                        case NSOrderedSame:
                            // 同一時刻
                            break;
                        case NSOrderedAscending:
                            [updObject setValue:[NSNumber numberWithBool:NO] forKey:@"on"];
                            break;
                        case NSOrderedDescending:
                            [updObject setValue:[NSNumber numberWithBool:NO] forKey:@"on"];
                            // nowよりotherDateのほうが過去
                            break;
                    }
                }
            }
            // saveメソッドでデータをDBに保存します。
            // エラー発生時には、引数に参照渡しで渡しているErrorオブジェクトの中身を表示します。
            NSError *error = nil;
            if (![self.managedObjectContext save:&error]) {
                NSLog(@"error = %@", error);
                
            } else {
                
                NSLog(@"Insert Completed.");
            }
            minDate = nil;
            break;
        }
    }
    
    if(bannerView == nil){
        GADBannerView *bannerView = [[GADBannerView alloc] initWithAdSize:kGADAdSizeLargeBanner];
        bannerView.adSize = kGADAdSizeLargeBanner;
        bannerView.adUnitID = @"ca-app-pub-8256083710740545/8580743719";
        bannerView.tag = 9999999;
        bannerView.rootViewController = tabBarController;
        [tabBarController.view addSubview:bannerView];
        [bannerView loadRequest:[GADRequest request]];
    }
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    bannerView.center = CGPointMake(screenSize.width/2, screenSize.height - tabBarController.tabBar.frame.size.height - (bannerView.frame.size.height / 2)  - 1);
}

-(void)viewWillDisappear:(BOOL)animated
{
    if([[self.fetchedResultsController fetchedObjects] count] > 0){
        self.navigationItem.hidesBackButton = NO;
        //	ナビゲーションバーの左ボタンを「編集」に変更。
        //	押された時に呼び出すメソッドもenterEditModeにする。
        //	テーブルビューを編集モードから離れさせる。
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"edit",nil) style:UIBarButtonItemStyleBordered target:self action:@selector(enterEditMode:)];
    }
    [self.tableView endUpdates];
    [self.tableView setEditing:NO animated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{   
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    return [sectionInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    
    if (!cell) {
        // 再利用できない場合は新規で作成
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1
                                      reuseIdentifier:CellIdentifier];
    }

    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

// Cell が選択された時
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath: (NSIndexPath*) indexPath{
    // toViewController
    selected_index = indexPath;
    edit_mode = YES;
    [self performSegueWithIdentifier:@"AlartViewUnwindSegue" sender:self];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSManagedObjectContext *context = [self.fetchedResultsController managedObjectContext];
        [context deleteObject:[self.fetchedResultsController objectAtIndexPath:indexPath]];
        
        if([[self.fetchedResultsController fetchedObjects] count] <= 1){
            self.navigationItem.leftBarButtonItem = nil;
            [self.tableView endUpdates];
            [self.tableView setEditing:NO animated:YES];
        }
        
        [self.tableView endUpdates];
        
        
//        NSError *error = nil;
//        if (![context save:&error]) {
//            // Replace this implementation with code to handle the error appropriately.
//            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
//            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
//            abort();
//        }
    }
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    itemArray = [NSMutableArray array];
    // The table view should not be re-orderable.
    return NO;
}

- (IBAction)alarmViewReturnActionForSegue:(UIStoryboardSegue *)segue
{
    if([[self.fetchedResultsController fetchedObjects] count] > 0){
        if(self.navigationItem.leftBarButtonItem == nil){
            self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"edit",nil) style:UIBarButtonItemStyleBordered target:self action:@selector(enterEditMode:)];
            
        }
    }
}

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    NSManagedObject *object = [self.fetchedResultsController objectAtIndexPath:indexPath];
    int hour = [[object valueForKey:@"hour"] intValue];
    int min = [[object valueForKey:@"minute"] intValue];
    NSString *label_str = [object valueForKey:@"label"];
    NSString *week_str = @"";
    
    if([[object valueForKey:@"sun"] intValue] == 1){
        week_str = [NSString stringWithFormat:@"%@%@%@",week_str,NSLocalizedString(@"s_sun",nil),@" "];
    }
    if([[object valueForKey:@"mon"] intValue] == 1){
        week_str = [NSString stringWithFormat:@"%@%@%@",week_str,NSLocalizedString(@"s_mon",nil),@" "];
    }
    if([[object valueForKey:@"tue"] intValue] == 1){
        week_str = [NSString stringWithFormat:@"%@%@%@",week_str,NSLocalizedString(@"s_tue",nil),@" "];
    }
    if([[object valueForKey:@"wed"] intValue] == 1){
        week_str = [NSString stringWithFormat:@"%@%@%@",week_str,NSLocalizedString(@"s_wed",nil),@" "];
    }
    if([[object valueForKey:@"thu"] intValue] == 1){
        week_str = [NSString stringWithFormat:@"%@%@%@",week_str,NSLocalizedString(@"s_thu",nil),@" "];
    }
    if([[object valueForKey:@"fri"] intValue] == 1){
        week_str = [NSString stringWithFormat:@"%@%@%@",week_str,NSLocalizedString(@"s_fri",nil),@" "];
    }
    if([[object valueForKey:@"sat"] intValue] == 1){
        week_str = [NSString stringWithFormat:@"%@%@%@",week_str,NSLocalizedString(@"s_sat",nil),@" "];
    }
    if(week_str.length > 0){
        label_str = [NSString stringWithFormat:@"%@%@",label_str,NSLocalizedString(@",",nil)];
    }

    if([[object valueForKey:@"mon"] intValue] == 1
       && [[object valueForKey:@"tue"] intValue] == 1
       && [[object valueForKey:@"wed"] intValue] == 1
       && [[object valueForKey:@"thu"] intValue] == 1
       && [[object valueForKey:@"fri"] intValue] == 1
       && [[object valueForKey:@"sat"] intValue] == 0
       && [[object valueForKey:@"sun"] intValue] == 0
       ){
        week_str  = NSLocalizedString(@"s_week",nil);
    }

    
    if([[object valueForKey:@"sun"] intValue] == 1
       && [[object valueForKey:@"mon"] intValue] == 1
       && [[object valueForKey:@"tue"] intValue] == 1
       && [[object valueForKey:@"wed"] intValue] == 1
       && [[object valueForKey:@"thu"] intValue] == 1
       && [[object valueForKey:@"fri"] intValue] == 1
       && [[object valueForKey:@"sat"] intValue] == 1
       ){
       week_str  = NSLocalizedString(@"s_every",nil);
    }
    
    week_str = [NSString stringWithFormat:@"%@%@",label_str, week_str];
    cell.textLabel.text = [[NSString stringWithFormat:@"%02d:%02d",hour, min] description];
    cell.detailTextLabel.text = week_str;
    cell.editingAccessoryType = UITableViewCellAccessoryDisclosureIndicator;
    // スイッチを作成して、tableCellに載せる。
    cell.accessoryType = UITableViewCellAccessoryNone;
    UISwitch *sw = [[UISwitch alloc] initWithFrame:CGRectZero];
    sw.tag = indexPath.row;
    // UISwitchはUIContolを継承しているので、タップされた際の動作を簡単に指定できる。
    [sw addTarget:self action:@selector(tapSwich:) forControlEvents:UIControlEventValueChanged];
    // accessoryViewに代入するときれいに動作します。
    cell.accessoryView = sw;
    
    [itemArray addObject:[object valueForKey:@"minute"]];
    if([[object valueForKey:@"on"] intValue]){
        [sw setOn:YES];
        cell.backgroundColor = [UIColor whiteColor];
        cell.textLabel.textColor = [UIColor darkGrayColor];
        cell.detailTextLabel.textColor = [UIColor darkGrayColor];
    }else{
        [sw setOn:NO animated:YES ];
        cell.backgroundColor = [UIColor lightGrayColor];
        cell.textLabel.textColor = [UIColor grayColor];
        cell.detailTextLabel.textColor = [UIColor grayColor];
    }
}

// UISwitchがタップされた際に呼び出されるメソッド。
-(void)tapSwich:(id)sender {
    id cell = sender;
    while (![cell isKindOfClass:[UITableViewCell class]]) {
        cell = [cell superview];
    }
    NSIndexPath* indexPath = [self.tableView indexPathForCell:cell];
    UISwitch *sw = (UISwitch *)sender;
    if(sw.on){
        int countOn = 0;
        for (int i = 0; i < [[_fetchedResultsController fetchedObjects] count]; i++) {
            NSNumber *on = [[[_fetchedResultsController fetchedObjects] valueForKey:@"on"] objectAtIndex:i];
            if([on intValue] == 1){
                countOn++;
            }
        }
        
        if(countOn > 1){
            UIAlertView *alert = [[UIAlertView alloc]
                                  initWithTitle:NSLocalizedString(@"AlarmNumTit",nil)
                                  message:NSLocalizedString(@"AlarmNumMes",nil)
                                  delegate:self
                                  cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
            alert.tag = 2;
            [alert show];
            [sw setOn:NO animated:NO];
        }else{
             NSManagedObject *updObject = [_fetchedResultsController.fetchedObjects objectAtIndex:indexPath.row];
            [updObject setValue:[NSNumber numberWithBool:YES] forKey:@"on"];
            
            // saveメソッドでデータをDBに保存します。
            // エラー発生時には、引数に参照渡しで渡しているErrorオブジェクトの中身を表示します。
            NSError *error = nil;
            if (![self.managedObjectContext save:&error]) {
                NSLog(@"error = %@", error);
                
            } else {
                UIAlertView *alert = [[UIAlertView alloc]
                                      initWithTitle:NSLocalizedString(@"check",nil)
                                      message:NSLocalizedString(@"checkMes",nil)
                                      delegate:self
                                      cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                alert.tag = 123456;
                [alert show];
                NSLog(@"Insert Completed.");
            }
        }
    }else{
        NSManagedObject *updObject = [_fetchedResultsController.fetchedObjects objectAtIndex:indexPath.row];
        [updObject setValue:[NSNumber numberWithBool:NO] forKey:@"on"];
        
        // saveメソッドでデータをDBに保存します。
        // エラー発生時には、引数に参照渡しで渡しているErrorオブジェクトの中身を表示します。
        NSError *error = nil;
        if (![self.managedObjectContext save:&error]) {
            NSLog(@"error = %@", error);
            
        } else {
            NSLog(@"Insert Completed.");
        }
    }
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([[segue identifier] isEqualToString:@"AlartViewUnwindSegue"]) {
        AlarmsetupViewController *alarmSetUpView = [segue destinationViewController];
        alarmSetUpView.editMode = edit_mode;
        alarmSetUpView.setupViewIndexpath = selected_index;
        alarmSetUpView.tabBarHeight = self.tabBarController.tabBar.frame.size.height;
    }else{
        [self leaveEditMode:(id)sender];
    }
}

- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    // Edit the entity name as appropriate.
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Alarm" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:20];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *hourDisc = [[NSSortDescriptor alloc] initWithKey:@"hour" ascending:YES];
    NSSortDescriptor *minuteDisc = [[NSSortDescriptor alloc] initWithKey:@"minute" ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObjects:hourDisc,minuteDisc, nil];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    // Edit the section name key path and cache name if appropriate.
    // nil for section name key path means "no sections".
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    aFetchedResultsController.delegate = self;
    self.fetchedResultsController = aFetchedResultsController;
    
	NSError *error = nil;
	if (![self.fetchedResultsController performFetch:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
	    NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
	    abort();
	}
    
    return _fetchedResultsController;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    UITableView *tableView = self.tableView;
    [itemArray removeAllObjects];
    for (int i = 0; i < [[_fetchedResultsController fetchedObjects] count]; i++) {
        [itemArray addObject:[[[_fetchedResultsController fetchedObjects] valueForKey:@"minute"] objectAtIndex:i]];
    }
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            if([[self.fetchedResultsController fetchedObjects] count] > 0){
                self.navigationItem.hidesBackButton = NO;
                //	ナビゲーションバーの左ボタンを「編集」に変更。
                //	押された時に呼び出すメソッドもenterEditModeにする。
                //	テーブルビューを編集モードから離れさせる。
                self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"edit",nil) style:UIBarButtonItemStyleBordered target:self action:@selector(enterEditMode:)];
                
                int countOn = 0;
                minDate = nil;
                for (int i = 0; i < [[self.fetchedResultsController  fetchedObjects] count]; i++) {
                    NSNumber *on = [[[self.fetchedResultsController  fetchedObjects] valueForKey:@"on"] objectAtIndex:i];
                    if([on intValue] == 1){
                        countOn++;
                    }
                    NSDate *createdat = [[[self.fetchedResultsController  fetchedObjects] valueForKey:@"createdat"] objectAtIndex:i];

                    if(minDate != nil){
                        NSComparisonResult result = [createdat compare:minDate];
                        switch (result) {
                            case NSOrderedSame:
                                // 同一時刻
                                break;
                            case NSOrderedAscending:
                                // nowよりotherDateのほうが未来
                                break;
                            case NSOrderedDescending:
                                minDate = createdat;
                                // nowよりotherDateのほうが過去
                                break;
                        }
                    }else{
                        minDate = createdat;
                    }
                }
                
                if(countOn > 2){
                    UIAlertView *alert = [[UIAlertView alloc]
                                          initWithTitle:NSLocalizedString(@"AlarmNumTit",nil)
                                          message:NSLocalizedString(@"AlarmNumMes",nil)
                                          delegate:self
                                          cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                    alert.tag = 1;
                    [alert show];
                }
            }
            [self.tableView endUpdates];
            [self.tableView setEditing:NO animated:YES];
            [self.tableView reloadData];
            //            if([[self.fetchedResultsController fetchedObjects] count] > 0){
            //                self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"edit",nil) style:UIBarButtonItemStyleBordered target:self action:@selector(enterEditMode:)];
            //            }else{
            //                self.navigationItem.leftBarButtonItem = nil;
            //                self.navigationItem.hidesBackButton = YES;
            //            }
            selected_index = 0;
            edit_mode = NO;
            break;
            
        case NSFetchedResultsChangeDelete:{
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            
            //            self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"done",nil) style:UIBarButtonItemStyleBordered target:self action:@selector(leaveEditMode:)];
            break;
        }
            
        case NSFetchedResultsChangeUpdate:{
            if([[self.fetchedResultsController fetchedObjects] count] > 0){
                self.navigationItem.hidesBackButton = NO;
                //	ナビゲーションバーの左ボタンを「編集」に変更。
                //	押された時に呼び出すメソッドもenterEditModeにする。
                //	テーブルビューを編集モードから離れさせる。
                self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"edit",nil) style:UIBarButtonItemStyleBordered target:self action:@selector(enterEditMode:)];
            }
            
            int countOn = 0;
            minDate = nil;
            for (int i = 0; i < [[self.fetchedResultsController  fetchedObjects] count]; i++) {
                NSNumber *on = [[[self.fetchedResultsController  fetchedObjects] valueForKey:@"on"] objectAtIndex:i];
                if([on intValue] == 1){
                    countOn++;
                }
                NSDate *createdat = [[[self.fetchedResultsController  fetchedObjects] valueForKey:@"createdat"] objectAtIndex:i];
                
                if(minDate != nil){
                    NSComparisonResult result = [createdat compare:minDate];
                    switch (result) {
                        case NSOrderedSame:
                            // 同一時刻
                            break;
                        case NSOrderedAscending:
                            // nowよりotherDateのほうが未来
                            break;
                        case NSOrderedDescending:
                            minDate = createdat;
                            // nowよりotherDateのほうが過去
                            break;
                    }
                }else{
                    minDate = createdat;
                }
            }
            
            if(countOn > 2){
                UIAlertView *alert = [[UIAlertView alloc]
                                      initWithTitle:NSLocalizedString(@"AlarmNumTit",nil)
                                      message:NSLocalizedString(@"AlarmNumMes",nil)
                                      delegate:self
                                      cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
                alert.tag = 1;
                [alert show];
            }
            
            [self.tableView endUpdates];
            [self.tableView setEditing:NO animated:YES];
            [self configureCell:[self.tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            [self.tableView reloadData];
            selected_index = 0;
            edit_mode = NO;

            break;
        }
        case NSFetchedResultsChangeMove:
            if([[self.fetchedResultsController fetchedObjects] count] > 0){
                self.navigationItem.hidesBackButton = NO;
                //	ナビゲーションバーの左ボタンを「編集」に変更。
                //	押された時に呼び出すメソッドもenterEditModeにする。
                //	テーブルビューを編集モードから離れさせる。
                self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"edit",nil) style:UIBarButtonItemStyleBordered target:self action:@selector(enterEditMode:)];
            }
            [self.tableView endUpdates];
            [self.tableView setEditing:NO animated:YES];
            [self configureCell:[self.tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            [self.tableView reloadData];
            selected_index = 0;
            edit_mode = NO;

//            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
//            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView endUpdates];
}

/*
 // Implementing the above methods to update the table view in response to individual changes may have performance implications if a large number of changes are made simultaneously. If this proves to be an issue, you can instead just implement controllerDidChangeContent: which notifies the delegate that all section and object changes have been processed.
 
 - (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
 {
 // In the simplest, most efficient, case, reload the table view.
 [self.tableView reloadData];
 }
 */

- (IBAction)enterEditMode:(id)sender {
    //	ビゲーションバーの左ボタンを「終了」に変更。押された時に
	//	テーブルビューを編集モード突入にさせる。
    self.navigationItem.hidesBackButton = NO;
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"done",nil) style:UIBarButtonItemStyleBordered target:self action:@selector(leaveEditMode:)];
	[self.tableView setEditing:YES animated:YES];
	[self.tableView beginUpdates];
    
}

- (IBAction)leaveEditMode:(id)sender {
    NSError *error = nil;
    if (![self.managedObjectContext save:&error]) {
        NSLog(@"error = %@", error);
        
    } else {
        NSLog(@"Insert Completed.");
    }

    if([[self.fetchedResultsController fetchedObjects] count] > 0){
        self.navigationItem.hidesBackButton = NO;
        //	ナビゲーションバーの左ボタンを「編集」に変更。
	    //	押された時に呼び出すメソッドもenterEditModeにする。
    	//	テーブルビューを編集モードから離れさせる。
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"edit",nil) style:UIBarButtonItemStyleBordered target:self action:@selector(enterEditMode:)];
    }
    //	テーブルビューを編集モードから離れさせる。
    [self.tableView endUpdates];
    [self.tableView setEditing:NO animated:YES];
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    // Detemine if it's in editing mode
    if (self.tableView.editing) {
        return UITableViewCellEditingStyleDelete;
    }
    return UITableViewCellEditingStyleNone;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)viewDidDisappear:(BOOL)animated
{
}
- (void)viewDidAppear:(BOOL)animated
{
    NSLog(@"viewDidAppear");
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    UITabBarController *tabBarController = (UITabBarController *)window.rootViewController;
    GADBannerView *bannerView = (GADBannerView*)[tabBarController.view viewWithTag:9999999];
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;    
    bannerView.center = CGPointMake(screenSize.width/2, screenSize.height - tabBar.size.height - (bannerView.frame.size.height / 2)  - 1);
}

@end
