//
//  AlarmsetupViewController.h
//  Naptimer
//
//  Created by 大口 尚紀(管理者) on 2014/07/15.
//  Copyright (c) 2014年 大口 尚紀(管理者). All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlarmsetupViewController : UIViewController
<UIPickerViewDelegate, NSFetchedResultsControllerDelegate>  // PickerViewのデータやタップ時のイベントのため
{
    NSMutableArray *minutes;    // 分
    NSMutableArray *hours;    //　時間
}
@property bool editMode;
@property NSIndexPath *setupViewIndexpath;
@property NSManagedObject *updObject;
@property double tabBarHeight;
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (weak, nonatomic) IBOutlet UINavigationBar *registNav;

@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;
@property (weak, nonatomic) IBOutlet UITableView *menu;
@property (nonatomic, strong) NSArray *dataSourcemenu;
- (IBAction)save:(id)sender;
@end
