//
//  AlarmsetupViewController.m
//  Naptimer
//
//  Created by 大口 尚紀(管理者) on 2014/07/15.
//  Copyright (c) 2014年 大口 尚紀(管理者). All rights reserved.
//

#import "AlarmsetupViewController.h"
#import "WeekendViewController.h"
#import "LabelViewController.h"
#import "AppDelegate.h"

@interface AlarmsetupViewController ()<UITableViewDelegate, UITableViewDataSource>
@end

@implementation AlarmsetupViewController{
    NSMutableArray *alarmview_week_arr;
    NSString *alarmview_label_text;
    int n_hour;
    int n_minute;
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    // Do any additional setup after loading the view.
    [super viewDidLoad];
    //  ナビゲーションバーのタイトルの色を白色に変更する
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectZero];
    titleLabel.font = [UIFont fontWithName:@"Helvetica" size:15];
    titleLabel.text = NSLocalizedString(@"addalarm",nil);
    titleLabel.textColor = [UIColor grayColor];
    [titleLabel sizeToFit];
    self.navigationItem.titleView = titleLabel;
    
    id appDelegate = (id)[[UIApplication sharedApplication] delegate];
    self.managedObjectContext = [appDelegate managedObjectContext];
    if(self.editMode){
       self.updObject = [self.fetchedResultsController objectAtIndexPath:self.setupViewIndexpath];
        n_hour = [[self.updObject valueForKey:@"hour"] intValue];
        n_minute = [[self.updObject valueForKey:@"minute"] intValue];
        alarmview_week_arr  =  [NSMutableArray arrayWithObjects:
                                [NSNumber numberWithBool:[[self.updObject valueForKey:@"sun"] boolValue]],
                                [NSNumber numberWithBool:[[self.updObject valueForKey:@"mon"] boolValue]],
                                [NSNumber numberWithBool:[[self.updObject valueForKey:@"tue"] boolValue]],
                                [NSNumber numberWithBool:[[self.updObject valueForKey:@"wed"] boolValue]],
                                [NSNumber numberWithBool:[[self.updObject valueForKey:@"thu"] boolValue]],
                                [NSNumber numberWithBool:[[self.updObject valueForKey:@"fri"] boolValue]],
                                [NSNumber numberWithBool:[[self.updObject valueForKey:@"sat"] boolValue]],
                                nil];
        alarmview_label_text = [self.updObject valueForKey:@"label"];
    }else{
        // 現在の日付を取得
        NSDate *now = [NSDate date];
    
        NSCalendar *calendar = [NSCalendar currentCalendar];
        NSUInteger flags;
        NSDateComponents *comps;
    
        flags = NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond;
        comps = [calendar components:flags fromDate:now];
    
        n_hour = (int)comps.hour;
        n_minute = (int)comps.minute;
        // 現在の日付(NSDate)から年と月をintで取得
        alarmview_week_arr  =  [NSMutableArray arrayWithObjects:
                                [NSNumber numberWithBool:NO],
                                [NSNumber numberWithBool:NO],
                                [NSNumber numberWithBool:NO],
                                [NSNumber numberWithBool:NO],
                                [NSNumber numberWithBool:NO],
                                [NSNumber numberWithBool:NO],
                                [NSNumber numberWithBool:NO],
                                nil];
        alarmview_label_text = @"";

    }
    
    // ラベルに表示
    self.dateLabel.text = [NSString stringWithFormat:@"%02d : %02d", (int)n_hour, (int)n_minute];
    self.dateLabel.font = [UIFont fontWithName:@"Helvetica" size:32];
    self.dateLabel.textColor = [UIColor grayColor];
    // Pickerviewにデリゲートを設定
    self.pickerView.delegate = self;
    
    minutes = [[NSMutableArray alloc] initWithCapacity:60];
    for (int i = 0; i <= 59; i++) {
        [minutes addObject:[NSString stringWithFormat:@"%d", i]];
    }
    
    hours =  [[NSMutableArray alloc] initWithCapacity:24];
    for (int j = 0; j < 24; j++) {
        [hours addObject:[NSString stringWithFormat:@"%d", j]];
    }
    
    
    // -------------------------------------------
    // PickerViewに初期の選択値として、現在の日付を設定
    // -------------------------------------------
    // PickerViewの列数を計算する
    // PickerViewの初期の選択値を設定
    // componentが行番号、selectRowが列番号
    [self.pickerView selectRow:n_minute inComponent:1 animated:YES];  // 分を選択
    [self.pickerView selectRow:n_hour inComponent:0 animated:YES];  // 時を選択
	// Do any additional setup after loading the view.
    // デリゲートメソッドをこのクラスで実装する
    self.menu.delegate = self;
    self.menu.dataSource = self;
    self.dataSourcemenu = @[NSLocalizedString(@"repeat",nil), NSLocalizedString(@"label",nil)];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ( [[segue identifier] isEqualToString:@"toSelectWeek"] ) {
        
        WeekendViewController *weekendController
        = (WeekendViewController*)[segue destinationViewController];
        
        // 遷移元ポインタを渡しておく
        weekendController.delegate = (id)self;
        weekendController.week_arr = alarmview_week_arr;
    }
    if ( [[segue identifier] isEqualToString:@"toInputLabel"] ) {
        
        LabelViewController *labelController
        = (LabelViewController*)[segue destinationViewController];
        
        // 遷移元ポインタを渡しておく
        labelController.delegate = (id)self;
        labelController.label_str = alarmview_label_text;
    }

}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
    // Dispose of any resources that can be recreated.
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    // 列を指定
    return 2;
}

// 列(component)に対する、行(row)の数を返す
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    switch (component) {
        case 1:
            return [minutes count];   // 分のデータ数を返す
        case 0:
            return [hours count];   // 時のデータ数を返す
    }
    return 0;
}

// 列(component)と行(row)に対応する文字列を返す
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    switch (component) {
        case 1:
            return [minutes objectAtIndex:row];   // 分のデータの列に対応した文字列を返す
        case 0:
            return [hours objectAtIndex:row];   // 年のデータの列に対応した文字列を返す
    }
    return nil;
}


// PickerViewの操作が行われたときに呼ばれる
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row  inComponent:(NSInteger)component {
    
    // PickerViewの選択されている年と月の列番号を取得
    int rowOfMinute  = (int)[pickerView selectedRowInComponent:1];
    int rowOfHour  = (int)[pickerView selectedRowInComponent:0];
    
    self.dateLabel.text = [NSString stringWithFormat:@"%02d : %02d",
                               [[hours objectAtIndex:rowOfHour] intValue], [[minutes objectAtIndex:rowOfMinute] intValue]];
    n_hour = [[hours objectAtIndex:rowOfHour] intValue];
    n_minute = [[minutes objectAtIndex:rowOfMinute] intValue];
    self.dateLabel.font = [UIFont fontWithName:@"Helvetica" size:32];
    self.dateLabel.textColor = [UIColor grayColor];
}

- (NSString *)pickerView:(UIPickerView *)pickerView getNowVlue:(NSInteger)row  inComponent:(NSInteger)component {
    
    // PickerViewの選択されている年と月の列番号を取得
    int rowOfMinute  = (int)[pickerView selectedRowInComponent:1]; // 年を取得
    int rowOfHour  = (int)[pickerView selectedRowInComponent:0]; // 年を取得
    
    switch (component){
        case 1:
            return [minutes objectAtIndex:rowOfMinute];
        case 0:
            return [hours objectAtIndex:rowOfHour];
    }
    return nil;
}

/**
 * 行のサイズを変更
 */
- (CGFloat)pickerView:(UIPickerView *)pickerView
    widthForComponent:(NSInteger)component
{
    switch (component) {
        case 0: // 1列目
            return 100.0;
            break;
            
        case 1: // 2列目
            return 100.0;
            break;
            
        default:
            return 0;
            break;
    }
}

- (UIView *)pickerView:(UIPickerView *)pickerView
            viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view {
    
    UILabel *label = (id)view;
    
    if (!label) {
        label= [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0.0f, [pickerView rowSizeForComponent:component].width, 216.0)];
    }
    label.text = minutes[row];
    label.font = [UIFont fontWithName:@"Helvetica" size:32];
    label.textColor = [UIColor grayColor];
    label.textAlignment = NSTextAlignmentCenter;
    
    
    return label;
}

/**
 テーブルに表示するデータ件数を返します。（必須）
 
 @return NSInteger : データ件数
 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger dataCount;
    
    // テーブルに表示するデータ件数を返す
    dataCount = self.dataSourcemenu.count;
    return dataCount;
}

/**
 テーブルに表示するセクション（区切り）の件数を返します。（オプション）
 
 @return NSInteger : セクションの数
 */
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

/**
 テーブルに表示するセルを返します。（必須）
 
 @return UITableViewCell : テーブルセル
 */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    // 再利用できるセルがあれば再利用する
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell) {
        // 再利用できない場合は新規で作成
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleValue1
                                      reuseIdentifier:CellIdentifier];
    }
    
    cell.textLabel.font = [UIFont fontWithName:@"Helvetica" size:15];
    cell.detailTextLabel.font = [UIFont fontWithName:@"Helvetica" size:15];
    cell.textLabel.text = self.dataSourcemenu[indexPath.row];
    cell.detailTextLabel.textColor = [UIColor lightGrayColor];
    
    if(self.editMode){
        NSString *week_str = @"";
        self.updObject = [self.fetchedResultsController objectAtIndexPath:self.setupViewIndexpath];
        
        if([[self.updObject valueForKey:@"sun"] intValue] == 1){
            week_str = [NSString stringWithFormat:@"%@%@%@",week_str,NSLocalizedString(@"s_sun",nil),@" "];
        }
        if([[self.updObject valueForKey:@"mon"] intValue] == 1){
            week_str = [NSString stringWithFormat:@"%@%@%@",week_str,NSLocalizedString(@"s_mon",nil),@" "];
        }
        if([[self.updObject valueForKey:@"tue"] intValue] == 1){
            week_str = [NSString stringWithFormat:@"%@%@%@",week_str,NSLocalizedString(@"s_tue",nil),@" "];
        }
        if([[self.updObject valueForKey:@"wed"] intValue] == 1){
            week_str = [NSString stringWithFormat:@"%@%@%@",week_str,NSLocalizedString(@"s_wed",nil),@" "];
        }
        if([[self.updObject valueForKey:@"thu"] intValue] == 1){
            week_str = [NSString stringWithFormat:@"%@%@%@",week_str,NSLocalizedString(@"s_thu",nil),@" "];
        }
        if([[self.updObject valueForKey:@"fri"] intValue] == 1){
            week_str = [NSString stringWithFormat:@"%@%@%@",week_str,NSLocalizedString(@"s_fri",nil),@" "];
        }
        if([[self.updObject valueForKey:@"sat"] intValue] == 1){
            week_str = [NSString stringWithFormat:@"%@%@%@",week_str,NSLocalizedString(@"s_sat",nil),@" "];
        }
        
        if([[self.updObject valueForKey:@"mon"] intValue] == 1
           && [[self.updObject valueForKey:@"tue"] intValue] == 1
           && [[self.updObject valueForKey:@"wed"] intValue] == 1
           && [[self.updObject valueForKey:@"thu"] intValue] == 1
           && [[self.updObject valueForKey:@"fri"] intValue] == 1
           && [[self.updObject valueForKey:@"sat"] intValue] == 0
           && [[self.updObject valueForKey:@"sun"] intValue] == 0
           ){
            week_str  = NSLocalizedString(@"s_week",nil);
        }
        
        if([[self.updObject valueForKey:@"sun"] intValue] == 1
           && [[self.updObject valueForKey:@"mon"] intValue] == 1
           && [[self.updObject valueForKey:@"tue"] intValue] == 1
           && [[self.updObject valueForKey:@"wed"] intValue] == 1
           && [[self.updObject valueForKey:@"thu"] intValue] == 1
           && [[self.updObject valueForKey:@"fri"] intValue] == 1
           && [[self.updObject valueForKey:@"sat"] intValue] == 1
           ){
            week_str  = NSLocalizedString(@"s_every",nil);
        }

        if([[self.updObject valueForKey:@"sun"] intValue] == 0
           && [[self.updObject valueForKey:@"mon"] intValue] == 0
           && [[self.updObject valueForKey:@"tue"] intValue] == 0
           && [[self.updObject valueForKey:@"wed"] intValue] == 0
           && [[self.updObject valueForKey:@"thu"] intValue] == 0
           && [[self.updObject valueForKey:@"fri"] intValue] == 0
           && [[self.updObject valueForKey:@"sat"] intValue] == 0
           ){
            week_str  = NSLocalizedString(@"naver",nil);
        }

        if(indexPath.row == 0){
            cell.detailTextLabel.text = week_str;
        }
        if(indexPath.row == 1){
            cell.detailTextLabel.text = [self.updObject valueForKey:@"label"];
        }
    }else{
        if(indexPath.row == 0){
            cell.detailTextLabel.text = NSLocalizedString(@"naver",nil);
        }
        if(indexPath.row == 1){
            cell.detailTextLabel.text = NSLocalizedString(@"alarm",nil);
        }
    }
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    return cell;
}

// Cell が選択された時
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath: (NSIndexPath*) indexPath{
    // toViewController
    if(indexPath.row == 0) {
        [self performSegueWithIdentifier:@"toSelectWeek" sender:self];
    } else if(indexPath.row == 1) {
        [self performSegueWithIdentifier:@"toInputLabel" sender:self];
        //[self performSegueWithIdentifier:@"toSelectWeek" sender:self];
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];    
}

//- (IBAction)alarmsetupReturnSegue:(UIStoryboardSegue *)segue
//{
//}

-(void)weekendViewController:(WeekendViewController *)weekendViewController didClose:(NSMutableArray *)week_arr
{
    alarmview_week_arr = week_arr;
    NSIndexPath* indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    UITableViewCell* cell = [self.menu cellForRowAtIndexPath:indexPath];
    bool every = YES;
    bool no_use = YES;
    bool ordinary = YES;
    NSString *week_str = @"";
    for(int i=0; i < week_arr.count; i++){
        if([week_arr objectAtIndex:i]==[NSNumber numberWithBool:YES]){
            switch(i){
                case 0:
                    week_str = [NSString stringWithFormat:@"%@%@%@",week_str,NSLocalizedString(@"s_sun",nil),@" "];
                    ordinary = NO;
                    break;
                case 1:
                    week_str = [NSString stringWithFormat:@"%@%@%@",week_str,NSLocalizedString(@"s_mon",nil),@" "];
                    break;
                case 2:
                    week_str = [NSString stringWithFormat:@"%@%@%@",week_str,NSLocalizedString(@"s_tue",nil),@" "];
                    break;
                case 3:
                    week_str = [NSString stringWithFormat:@"%@%@%@",week_str,NSLocalizedString(@"s_wed",nil),@" "];
                    break;
                case 4:
                    week_str = [NSString stringWithFormat:@"%@%@%@",week_str,NSLocalizedString(@"s_thu",nil),@" "];
                    break;
                case 5:
                    week_str = [NSString stringWithFormat:@"%@%@%@",week_str,NSLocalizedString(@"s_fri",nil),@" "];
                    break;
                case 6:
                    week_str = [NSString stringWithFormat:@"%@%@%@",week_str,NSLocalizedString(@"s_sat",nil),@" "];
                    ordinary = NO;
                    break;
                default:
                    break;
            }
            no_use=NO;
        } else{
            if(i==1 || i== 2 || i== 3 || i== 4 || i== 5){
                ordinary = NO;
            }
            every=NO;
        }
    }
    if(every == YES){
        week_str = NSLocalizedString(@"s_every",nil);
    }else if(ordinary == YES){
        week_str = NSLocalizedString(@"s_week",nil);
    }else if(no_use == YES){
        week_str = NSLocalizedString(@"naver",nil);
    }
    cell.detailTextLabel.text = week_str;
}

-(void)labelViewController:(LabelViewController *)labelViewController didCloseLabel:(NSString *)label_text
{
    NSLog(@"%@", label_text);
    alarmview_label_text = label_text;
    NSIndexPath* indexPath = [NSIndexPath indexPathForRow:1 inSection:0];
    UITableViewCell* cell = [self.menu cellForRowAtIndexPath:indexPath];
    if(label_text.length > 0){
        cell.detailTextLabel.text = label_text;
    } else{
        cell.detailTextLabel.text = NSLocalizedString(@"alarm",nil);
    }
}

- (IBAction)save:(id)sender {
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    UITabBarController *tabBarController = (UITabBarController *)window.rootViewController;
    GADBannerView *bannerView = (GADBannerView*)[tabBarController.view viewWithTag:9999999];
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    NSLog(@"%f", self.tabBarHeight);
    bannerView.center = CGPointMake(screenSize.width/2, screenSize.height - self.tabBarHeight - (bannerView.frame.size.height / 2)  - 1);

    if(self.editMode){
        [self updateObject:(id)sender];
    }else{
        [self insertNewObject:(id)sender];
    }
    
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle:NSLocalizedString(@"check",nil)
                          message:NSLocalizedString(@"checkMes",nil)
                          delegate:self
                          cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
    alert.tag = 123456;
    [alert show];
    
    [self performSegueWithIdentifier:@"AlartViewUnwindSegue" sender:self];
}

- (void)updateObject:(id)sender
{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }

    [self.updObject setValue:[NSDate date] forKey:@"createdat"];
    [self.updObject setValue:[alarmview_week_arr objectAtIndex:0] forKey:@"sun"];
    [self.updObject setValue:[alarmview_week_arr objectAtIndex:1] forKey:@"mon"];
    [self.updObject setValue:[alarmview_week_arr objectAtIndex:2] forKey:@"tue"];
    [self.updObject setValue:[alarmview_week_arr objectAtIndex:3] forKey:@"wed"];
    [self.updObject setValue:[alarmview_week_arr objectAtIndex:4] forKey:@"thu"];
    [self.updObject setValue:[alarmview_week_arr objectAtIndex:5] forKey:@"fri"];
    [self.updObject setValue:[alarmview_week_arr objectAtIndex:6] forKey:@"sat"];
    [self.updObject setValue:[NSNumber numberWithBool:YES] forKey:@"on"];
    [self.updObject setValue:[NSNumber numberWithInt:n_hour]  forKey:@"hour"];
    if(alarmview_label_text.length > 0){
        [self.updObject setValue:alarmview_label_text forKey:@"label"];
    }else{
        [self.updObject setValue:NSLocalizedString(@"alarm",nil) forKey:@"label"];
    }
    
    [self.updObject setValue:[NSNumber numberWithInt:n_minute] forKey:@"minute"];
    // Save the context.
    NSError *error = nil;
    if (![context save:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }else{
        NSLog(@"データ更新");
    }

}
- (void)insertNewObject:(id)sender
{
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    NSManagedObject *newManagedObject = [NSEntityDescription insertNewObjectForEntityForName:@"Alarm"  inManagedObjectContext:context];
    
    // If appropriate, configure the new managed object.
    // Normally you should use accessor methods, but using KVC here avoids the need to add a custom class to the template.
        [newManagedObject setValue:[NSDate date] forKey:@"createdat"];
        [newManagedObject setValue:[alarmview_week_arr objectAtIndex:0] forKey:@"sun"];
        [newManagedObject setValue:[alarmview_week_arr objectAtIndex:1] forKey:@"mon"];
        [newManagedObject setValue:[alarmview_week_arr objectAtIndex:2] forKey:@"tue"];
        [newManagedObject setValue:[alarmview_week_arr objectAtIndex:3] forKey:@"wed"];
        [newManagedObject setValue:[alarmview_week_arr objectAtIndex:4] forKey:@"thu"];
        [newManagedObject setValue:[alarmview_week_arr objectAtIndex:5] forKey:@"fri"];
        [newManagedObject setValue:[alarmview_week_arr objectAtIndex:6] forKey:@"sat"];
        [newManagedObject setValue:[NSNumber numberWithBool:YES] forKey:@"on"];
        [newManagedObject setValue:[NSNumber numberWithInt:n_hour]  forKey:@"hour"];
        if(alarmview_label_text.length > 0){
            [newManagedObject setValue:alarmview_label_text forKey:@"label"];
        }else{
            [newManagedObject setValue:NSLocalizedString(@"alarm",nil) forKey:@"label"];
        }

        [newManagedObject setValue:[NSNumber numberWithInt:n_minute] forKey:@"minute"];
    // Save the context.
    NSError *error = nil;
    if (![context save:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }else{
        NSLog(@"データ追加");
    }
    
}

- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    // Edit the entity name as appropriate.
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Alarm" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:20];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *hourDisc = [[NSSortDescriptor alloc] initWithKey:@"hour" ascending:YES];
    NSSortDescriptor *minuteDisc = [[NSSortDescriptor alloc] initWithKey:@"minute" ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObjects:hourDisc,minuteDisc, nil];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    // Edit the section name key path and cache name if appropriate.
    // nil for section name key path means "no sections".
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    aFetchedResultsController.delegate = self;
    self.fetchedResultsController = aFetchedResultsController;
    
	NSError *error = nil;
	if (![self.fetchedResultsController performFetch:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
	    NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
	    abort();
	}
    
    return _fetchedResultsController;
}

- (void)viewWillAppear:(BOOL)animated
{
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    UITabBarController *tabBarController = (UITabBarController *)window.rootViewController;
    GADBannerView *bannerView = (GADBannerView*)[tabBarController.view viewWithTag:9999999];

    CGRect bannerFrame = bannerView.frame;
    bannerFrame.origin.y = self.view.bounds.size.height - bannerFrame.size.height - 1;
    bannerView.frame = bannerFrame;
}

- (void)viewDidDisappear:(BOOL)animated
{
//    UIWindow *window = [UIApplication sharedApplication].keyWindow;
//    UITabBarController *tabBarController = (UITabBarController *)window.rootViewController;
//    GADBannerView *bannerView = (GADBannerView*)[tabBarController.view viewWithTag:9999999];
////    if(bannerView == nil){
////        GADBannerView *bannerView = [[GADBannerView alloc] initWithAdSize:kGADAdSizeLargeBanner];
////        bannerView.adSize = kGADAdSizeLargeBanner;
////        bannerView.adUnitID = @"ca-app-pub-8256083710740545/8580743719";
////        bannerView.tag = 9999999;
////        bannerView.rootViewController = tabBarController;
////        [tabBarController.view addSubview:bannerView];
////        [bannerView loadRequest:[GADRequest request]];
////    }
//    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
//    bannerView.center = CGPointMake(screenSize.width/2, screenSize.height - tabBarController.tabBar.frame.size.height - (bannerView.frame.size.height / 2)  - 1);
}

- (void)viewDidAppear:(BOOL)animated
{
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
