//
//  AlertViewController.m
//  Naptimer
//
//  Created by 大口 尚紀(管理者) on 2014/07/23.
//  Copyright (c) 2014年 大口 尚紀(管理者). All rights reserved.
//
#import "AlertViewController.h"
#import "AppDelegate.h"

@interface AlertViewController ()

@end

@implementation AlertViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    GADBannerView *bannerView = [[GADBannerView alloc] initWithAdSize:kGADAdSizeMediumRectangle];
    bannerView.adUnitID = @"ca-app-pub-8256083710740545/3964134971";
    bannerView.rootViewController = self.view;
    bannerView.tag = 9999999;
    [self.view addSubview:bannerView];
    [bannerView loadRequest:[GADRequest request]];
    
    UIColor *color = [UIColor colorWithRed:0.0 green:0.0 blue:0.0 alpha:0.5];
    self.view.backgroundColor = color;
    [self.closeButton setTitle:NSLocalizedString(@"close",nil) forState:UIControlStateNormal];
    [self.closeButton setBackgroundColor:[UIColor whiteColor]];
    
    bannerView.center = CGPointMake(self.view.frame.size.width/2, self.view.frame.size.height/2);
    [self.closeButton setFrame:CGRectMake(0, 0, bannerView.frame.size.width + 2, bannerView.frame.size.height/1.618/1.618/1.618)];
    [self.closeButton setBackgroundColor:[UIColor whiteColor]];
    self.closeButton.layer.borderColor = [[UIColor darkGrayColor]CGColor];
    self.closeButton.layer.borderWidth = 1.5;
    self.closeButton.layer.cornerRadius = 5.0;
    [self.closeButton setCenter:CGPointMake(self.view.frame.size.width/2, bannerView.frame.size.height/2 + self.view.frame.size.height/2 + self.closeButton.frame.size.height/2)];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
}

- (void)viewWillDisappear:(BOOL)animated
{
}

- (void)viewDidAppear:(BOOL)animated
{
    
}
- (IBAction)dismissAlert:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
@end
