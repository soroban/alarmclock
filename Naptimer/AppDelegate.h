//
//  AppDelegate.h
//  Naptimer
//
//  Created by 大口 尚紀(管理者) on 2014/05/07.
//  Copyright (c) 2014年 大口 尚紀(管理者). All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h> 
#import <CoreMotion/CoreMotion.h>
@import Firebase;
@import GoogleMobileAds;

@interface AppDelegate : UIResponder <UIApplicationDelegate>{
    //ここに変数を定義する
    int time;
}

@property (strong, nonatomic) UIWindow *window;
@property(nonatomic, assign)  int time;
@property(nonatomic, assign)  bool on_alarm;
@property(nonatomic, assign)  bool on_timer;
@property(nonatomic, assign)  double percent;
@property(nonatomic, assign)  double sub;
@property(nonatomic, assign)  NSArray *timer_arr;
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;
@property (strong, nonatomic) NSMutableArray *itemArray;
@property (retain, nonatomic) AVAudioPlayer *player;
@property(nonatomic, strong) CMMotionManager *motionManager;
@property(nonatomic, strong) GADBannerView *bannerView;

- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

@end
