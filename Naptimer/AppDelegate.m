//
//  AppDelegate.m
//  Naptimer
//
//  Created by 大口 尚紀(管理者) on 2014/05/07.
//  Copyright (c) 2014年 大口 尚紀(管理者). All rights reserved.
//

#import "AppDelegate.h"

#import "MasterViewController.h"
#import "MoveViewController.h"
#import "AlarmViewController.h"
#import "SetupViewController.h"
#import "DetailViewController.h"

@implementation AppDelegate{
    UILocalNotification *notification;
    UILocalNotification *notification_10;
    UILocalNotification *notification_20;
    UILocalNotification *notification_30;
    UILocalNotification *notification_40;
    UILocalNotification *notification_50;
    Boolean *compFlg;
}
@synthesize time;
@synthesize on_alarm;
@synthesize on_timer;
@synthesize managedObjectContext = _managedObjectContext;
@synthesize managedObjectModel = _managedObjectModel;
@synthesize persistentStoreCoordinator = _persistentStoreCoordinator;


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    
    [FIRApp configure];
    NSDictionary* userInfo = [launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey];
    [self.player stop];
    if (userInfo != nil) {
        UILocalNotification *notify = [launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey];
        NSCalendar *calendar = [NSCalendar currentCalendar];
        
        // 日時をカレンダーで年月日時分秒に分解する
        NSDateComponents *dateComps
        = [calendar components:NSCalendarUnitYear   |
           NSCalendarUnitMonth  |
           NSCalendarUnitDay    |
           NSCalendarUnitHour   |
           NSCalendarUnitMinute |
           NSCalendarUnitSecond
                      fromDate:notify.fireDate];
        
        //タイマーをセットする
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        
        // 対象エンティティを指定します。
        NSEntityDescription *entity
        = [NSEntityDescription entityForName:@"Alarm" inManagedObjectContext:self.managedObjectContext];
        [fetchRequest setEntity:entity];
        
        // 検索条件を指定します。
        NSPredicate *pred
        = [NSPredicate predicateWithFormat:@"on = YES and hour= %ld and minute = %ld", dateComps.hour, dateComps.minute];
        [fetchRequest setPredicate:pred];
        // ソート条件を指定します。
        NSSortDescriptor *hourDisc = [[NSSortDescriptor alloc] initWithKey:@"hour" ascending:YES];
        NSSortDescriptor *minuteDisc = [[NSSortDescriptor alloc] initWithKey:@"minute" ascending:YES];
        NSArray *sortDescriptors = [NSArray arrayWithObjects:hourDisc,minuteDisc, nil];
        [fetchRequest setSortDescriptors:sortDescriptors];
        NSArray *result= [self.managedObjectContext executeFetchRequest:fetchRequest error:nil];
        
        for (int i = 0; i < result.count; i++) {
            id obj = [result objectAtIndex: i];
            int sun = [[obj valueForKey:@"sun"] intValue];
            int mon = [[obj valueForKey:@"mon"] intValue];
            int tue = [[obj valueForKey:@"tue"] intValue];
            int wed = [[obj valueForKey:@"wed"] intValue];
            int thu = [[obj valueForKey:@"thu"] intValue];
            int fri = [[obj valueForKey:@"fri"] intValue];
            int sat = [[obj valueForKey:@"sat"] intValue];
            
            if(sun == 0 && mon == 0 && tue == 0 && wed == 0 && thu == 0 && fri == 0 && sat ==0){
                [obj setValue:[NSNumber numberWithBool:NO] forKey:@"on"];
                NSError *error = nil;
                if (![self.managedObjectContext save:&error]) {
                // Replace this implementation with code to handle the error appropriately.
                // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
                    abort();
                }else{
                    NSLog(@"データ追加");
                }
            }
        }
        
        UITabBarController *tbc = (UITabBarController *)self.window.rootViewController;
        UINavigationController *navigationController = tbc.viewControllers[0];
        NSString* viewIdentifier = @"moveView";
        UIStoryboard* sb = [[[self window] rootViewController] storyboard];
        UIViewController* vc = [[UIViewController alloc] init];
        vc = [sb instantiateViewControllerWithIdentifier:viewIdentifier];
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
        [self.window makeKeyAndVisible];
        [navigationController presentViewController:nav animated:NO completion:nil];
        
        on_alarm = true;
    }

    // NSUserDefaultsの取得
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    // KEY_BOOLの内容を取得し、BOOL型変数へ格納
    BOOL isBool = [defaults boolForKey:@"KEY_BOOL"];
    
    BOOL isSet = NO;
    if ([application respondsToSelector:@selector(registerUserNotificationSettings:)]) {
        isSet = YES;
    }
//    isBool = NO;
    // isBoolがNOの場合、アラート表示
    if (!isBool && isSet) {
        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
        UIViewController *viewController;
        viewController = [storyboard instantiateViewControllerWithIdentifier: @"settingView"];
        UINavigationController *nac =
        [[UINavigationController alloc] initWithRootViewController:viewController];
      
//        UIViewController *viewController;
//        UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
//        viewController = [storyboard instantiateViewControllerWithIdentifier: @"firstView"];
        
        self.window.rootViewController = nac;
        [self.window makeKeyAndVisible];
        
        // KEY_BOOLにYESを設定
        [defaults setBool:YES forKey:@"KEY_BOOL"];
        // 設定を保存
        [defaults synchronize];
    }else{
        UITabBarController *tbc = (UITabBarController *)self.window.rootViewController;
        UINavigationController *navigationController = tbc.viewControllers[0];
        MasterViewController *controller = (MasterViewController *)navigationController.topViewController;
        controller.managedObjectContext = self.managedObjectContext;
    }
    
    UITabBarController *tabBarController = (UITabBarController*)self.window.rootViewController;
    // Specify the ad's "unit identifier." This is your AdMob Publisher ID.
    self.bannerView = [[GADBannerView alloc] initWithAdSize:kGADAdSizeLargeBanner];
    self.bannerView.adSize = kGADAdSizeLargeBanner;
    CGRect rect1 = [[UIScreen mainScreen] bounds];
    self.bannerView.adUnitID = @"ca-app-pub-8256083710740545/8580743719";
    self.bannerView.tag = 9999999;
    // Let the runtime know which UIViewController to restore after taking
    // the user wherever the ad goes and add it to the view hierarchy.
    self.bannerView.rootViewController = tabBarController;
    [tabBarController.view addSubview:self.bannerView];
    
    // Initiate a generic request to load it with an ad.
    [self.bannerView loadRequest:[GADRequest request]];
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    //self.bannerView.center = CGPointMake(rect1.size.width/2, screenSize.height - tabBarController.tabBar.frame.size.height - (self.bannerView.frame.size.height / 2)  - 1);
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to paunormal_alarmse the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    [self.player stop];
    UITabBarController *tbc = (UITabBarController *)self.window.rootViewController;
    UINavigationController *navigationController = tbc.viewControllers[0];
    
    NSString* viewIdentifier = @"CompleteView";
    UIStoryboard* sb = [[[self window] rootViewController] storyboard];
    UIViewController* vc = [[UIViewController alloc] init];
    vc = [sb instantiateViewControllerWithIdentifier:viewIdentifier];
    [navigationController dismissViewControllerAnimated:YES completion:nil];
    
    //
    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    if(on_alarm == true){
        NSDate *now = [NSDate date];
        
        notification = [[UILocalNotification alloc] init];
        notification.fireDate = [now dateByAddingTimeInterval:+1];
        notification.hasAction = YES;  //アクションボタンの表示
        notification.alertBody = NSLocalizedString(@"timesup",nil);  //アラートのメッセージ本文
        notification.alertAction = NSLocalizedString(@"godmor2",nil);  //アクションボタンに表示する文字
        notification.soundName = @"normal_alarm_short.caf";  //アラート表示時に鳴らすサウンドファイル
        notification.repeatInterval = NSCalendarUnitMinute;
        notification.userInfo = [NSDictionary dictionaryWithObject:@"Recevied." forKey:@"kExtra"];
        [[UIApplication sharedApplication] scheduleLocalNotification:notification];
        
        
        notification_10 = [[UILocalNotification alloc] init];
        // 通知を登録する
        notification_10.fireDate = [now dateByAddingTimeInterval:+10];
        notification_10.hasAction = YES;  //アクションボタンの表示
        notification_10.alertBody = NSLocalizedString(@"timesup",nil);  //アラートのメッセージ本文
        notification_10.alertAction = NSLocalizedString(@"godmor2",nil);  //アクションボタンに表示する文字
        notification_10.soundName = @"normal_alarm_short.caf";  //アラート表示時に鳴らすサウンドファイル
        notification_10.repeatInterval = NSCalendarUnitMinute;
        notification_10.userInfo = [NSDictionary dictionaryWithObject:@"Recevied." forKey:@"kExtra"];
        // 通知を登録する
        [[UIApplication sharedApplication] scheduleLocalNotification:notification_10];
    

        notification_20 = [[UILocalNotification alloc] init];
        // 通知を登録する
        notification_20.fireDate = [now dateByAddingTimeInterval:+20];
        notification_20.hasAction = YES;  //アクションボタンの表示
        notification_20.alertBody = NSLocalizedString(@"timesup",nil);  //アラートのメッセージ本文
        notification_20.alertAction = NSLocalizedString(@"godmor2",nil);  //アクションボタンに表示する文字
        notification_20.soundName = @"normal_alarm_short.caf";  //アラート表示時に鳴らすサウンドファイル
        notification_20.repeatInterval = NSCalendarUnitMinute;
        notification_20.userInfo = [NSDictionary dictionaryWithObject:@"Recevied." forKey:@"kExtra"];
        // 通知を登録する
        [[UIApplication sharedApplication] scheduleLocalNotification:notification_20];

        
        
        notification_30 = [[UILocalNotification alloc] init];
        // 通知を登録する
        notification_30.fireDate = [now dateByAddingTimeInterval:+30];
        notification_30.hasAction = YES;  //アクションボタンの表示
        notification_30.alertBody = NSLocalizedString(@"timesup",nil);  //アラートのメッセージ本文
        notification_30.alertAction = NSLocalizedString(@"godmor2",nil);  //アクションボタンに表示する文字
        notification_30.soundName = @"normal_alarm_short.caf";  //アラート表示時に鳴らすサウンドファイル
        notification_30.repeatInterval = NSCalendarUnitMinute;
        notification_30.userInfo = [NSDictionary dictionaryWithObject:@"Recevied." forKey:@"kExtra"];
        // 通知を登録する
        [[UIApplication sharedApplication] scheduleLocalNotification:notification_30];
        
        
        
        notification_40 = [[UILocalNotification alloc] init];
        // 通知を登録する
        notification_40.fireDate = [now dateByAddingTimeInterval:+40];
        notification_40.hasAction = YES;  //アクションボタンの表示
        notification_40.alertBody = NSLocalizedString(@"timesup",nil);  //アラートのメッセージ本文
        notification_40.alertAction = NSLocalizedString(@"godmor2",nil);  //アクションボタンに表示する文字
        notification_40.soundName = @"normal_alarm_short.caf";  //アラート表示時に鳴らすサウンドファイル
        notification_40.repeatInterval = NSCalendarUnitMinute;
        notification_40.userInfo = [NSDictionary dictionaryWithObject:@"Recevied." forKey:@"kExtra"];
        // 通知を登録する
        [[UIApplication sharedApplication] scheduleLocalNotification:notification_40];

        
        notification_50 = [[UILocalNotification alloc] init];
        // 通知を登録する
        notification_50.fireDate = [now dateByAddingTimeInterval:+50];
        notification_50.hasAction = YES;  //アクションボタンの表示
        notification_50.alertBody = NSLocalizedString(@"timesup",nil);  //アラートのメッセージ本文
        notification_50.alertAction = NSLocalizedString(@"godmor2",nil);  //アクションボタンに表示する文字
        notification_50.soundName = @"normal_alarm_short.caf";  //アラート表示時に鳴らすサウンドファイル
        notification_50.repeatInterval = NSCalendarUnitMinute;
        notification_50.userInfo = [NSDictionary dictionaryWithObject:@"Recevied." forKey:@"kExtra"];
        // 通知を登録する
        [[UIApplication sharedApplication] scheduleLocalNotification:notification_50];

    }else if(on_timer == true){
        NSDate *now = [NSDate date];
        
        notification = [[UILocalNotification alloc] init];
        notification.fireDate = [now dateByAddingTimeInterval:time];
        notification.hasAction = YES;  //アクションボタンの表示
        notification.alertBody = NSLocalizedString(@"timesup",nil);  //アラートのメッセージ本文
        notification.alertAction = NSLocalizedString(@"godmor2",nil);  //アクションボタンに表示する文字
        notification.soundName = @"normal_alarm_middle.caf";  //アラート表示時に鳴らすサウンドファイル
        notification.repeatInterval = NSCalendarUnitMinute;
        notification.userInfo = [NSDictionary dictionaryWithObject:@"Recevied." forKey:@"kExtra"];
        [[UIApplication sharedApplication] scheduleLocalNotification:notification];
        
        notification_20 = [[UILocalNotification alloc] init];
        // 通知を登録する
        notification_20.fireDate = [now dateByAddingTimeInterval:time+20];
        notification_20.hasAction = YES;  //アクションボタンの表示
        notification_20.alertBody = NSLocalizedString(@"timesup",nil);  //アラートのメッセージ本文
        notification_20.alertAction = NSLocalizedString(@"godmor2",nil);  //アクションボタンに表示する文字
        notification_20.soundName = @"normal_alarm_middle.caf";  //アラート表示時に鳴らすサウンドファイル
        notification_20.repeatInterval = NSCalendarUnitMinute;
        notification_20.userInfo = [NSDictionary dictionaryWithObject:@"Recevied." forKey:@"kExtra"];
        // 通知を登録する
        [[UIApplication sharedApplication] scheduleLocalNotification:notification_20];
        
        notification_40 = [[UILocalNotification alloc] init];
        // 通知を登録する
        notification_40.fireDate = [now dateByAddingTimeInterval:time+40];
        notification_40.hasAction = YES;  //アクションボタンの表示
        notification_40.alertBody = NSLocalizedString(@"timesup",nil);  //アラートのメッセージ本文
        notification_40.alertAction = NSLocalizedString(@"godmor2",nil);  //アクションボタンに表示する文字
        notification_40.soundName = @"normal_alarm_middle.caf";  //アラート表示時に鳴らすサウンドファイル
        notification_40.repeatInterval = NSCalendarUnitMinute;
        notification_40.userInfo = [NSDictionary dictionaryWithObject:@"Recevied." forKey:@"kExtra"];
        // 通知を登録する
        [[UIApplication sharedApplication] scheduleLocalNotification:notification_40];

    }
    
    
//    else if(on_alarm == false){
        //タイマーをセットする
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        
        // 対象エンティティを指定します。
        NSEntityDescription *entity
        = [NSEntityDescription entityForName:@"Alarm" inManagedObjectContext:self.managedObjectContext];
        [fetchRequest setEntity:entity];
        
        // 検索条件を指定します。
        NSPredicate *pred
        = [NSPredicate predicateWithFormat:@"on = YES and (sun = YES or mon = YES or tue = YES or wed = YES or thu = YES or fri = YES or sat = YES)"];
        [fetchRequest setPredicate:pred];
        // ソート条件を指定します。
        NSSortDescriptor *hourDisc = [[NSSortDescriptor alloc] initWithKey:@"hour" ascending:YES];
        NSSortDescriptor *minuteDisc = [[NSSortDescriptor alloc] initWithKey:@"minute" ascending:YES];
        NSArray *sortDescriptors = [NSArray arrayWithObjects:hourDisc,minuteDisc, nil];
        [fetchRequest setSortDescriptors:sortDescriptors];
        NSArray *result= [self.managedObjectContext executeFetchRequest:fetchRequest error:nil];
        
        // 繰り返ししない
        //検索条件を指定します。
        pred
        = [NSPredicate predicateWithFormat:@"on = YES and sun = NO and mon =  NO and tue =  NO and wed =  NO and thu =  NO and fri =  NO and sat = NO"];
        [fetchRequest setPredicate:pred];
        [fetchRequest setSortDescriptors:sortDescriptors];
        NSArray *no_repeat_result= [self.managedObjectContext executeFetchRequest:fetchRequest error:nil];
        
        // 現在日付を取得
        NSDate *now = [NSDate date];
        NSCalendar *calendar = [NSCalendar currentCalendar];
        NSDateComponents *comps;
        NSUInteger flags;
        
        // 年・月・日を取得
        flags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay;
        comps = [calendar components:flags fromDate:now];
        
        NSInteger year = comps.year;
        NSInteger month = comps.month;
        NSInteger day = comps.day;
        
        // 時・分・秒を取得
        flags = NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond;
        comps = [calendar components:flags fromDate:now];
        
        NSInteger n_hour = comps.hour;
        NSInteger n_minute = comps.minute;
        NSDateComponents *n_comps = [[NSDateComponents alloc]init];
        // 1日後とする
        [n_comps setDay:1];

        // 1日後のNSDateインスタンスを取得する
        NSDate *date = [[NSCalendar currentCalendar] dateByAddingComponents:n_comps toDate:[NSDate date] options:0];
        flags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay;
        comps = [calendar components:flags fromDate:date];
        NSInteger nextyear = comps.year;
        NSInteger nextmonth = comps.month;
        NSInteger nextday = comps.day;

        for (int i = 0; i < no_repeat_result.count; i++) {
            id obj = [no_repeat_result objectAtIndex: i];
            NSInteger d_hour = [[obj valueForKey:@"hour"] intValue];
            NSInteger d_minute = [[obj valueForKey:@"minute"] intValue];
            UILocalNotification *notify = [[UILocalNotification alloc] init];
            UILocalNotification *notify_20 = [[UILocalNotification alloc] init];
            UILocalNotification *notify_40 = [[UILocalNotification alloc] init];
            //今日に設定
            if(n_hour < d_hour || (n_hour == d_hour && n_minute < d_minute) ){
                NSCalendar *new_calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
                NSDate *new_now = [NSDate date];
                NSDateComponents *componentsForFireDate = [new_calendar components:(NSCalendarUnitYear | NSCalendarUnitWeekday | NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond | NSCalendarUnitWeekday) fromDate: new_now];
                // 時間帯を固定
                [componentsForFireDate setYear: year];
                [componentsForFireDate setMonth: month];
                [componentsForFireDate setDay: day];
                [componentsForFireDate setHour: d_hour];
                [componentsForFireDate setMinute:d_minute];
                [componentsForFireDate setSecond:1];
                [componentsForFireDate setTimeZone:[NSTimeZone systemTimeZone]];
                
                // 分ごとに
                notify.repeatInterval = NSCalendarUnitMinute;
                notify.alertBody = NSLocalizedString(@"timesup",nil);  //アラートのメッセージ本文
                notify.alertAction = NSLocalizedString(@"godmor2",nil);  //アクションボタンに表示する文字
                notify.soundName = @"normal_alarm_middle.caf";  //アラート表示時に鳴らすサウンドファイル
                notify.fireDate = [new_calendar dateFromComponents:componentsForFireDate];
                notify.timeZone = [NSTimeZone systemTimeZone];
                notify.userInfo = [NSDictionary dictionaryWithObjectsAndKeys:
                                   [[NSString alloc] initWithFormat:@"%d",(int)d_hour], @"hour",
                                   [[NSString alloc] initWithFormat:@"%d",(int)d_minute],@"minute",
                                   [[NSString alloc] initWithFormat:@"%d",(int)1],@"sec",
                                                            @"YES", @"ok",nil];
                [[UIApplication sharedApplication] scheduleLocalNotification:notify];

                [componentsForFireDate setSecond:20];
                // 分ごとに
                notify_20.repeatInterval = NSCalendarUnitMinute;
                notify_20.alertBody = NSLocalizedString(@"timesup",nil);  //アラートのメッセージ本文
                notify_20.alertAction = NSLocalizedString(@"godmor2",nil);  //アクションボタンに表示する文字
                notify_20.soundName = @"normal_alarm_middle.caf";  //アラート表示時に鳴らすサウンドファイル
                notify_20.fireDate = [new_calendar dateFromComponents:componentsForFireDate];
                notify_20.timeZone = [NSTimeZone systemTimeZone];
                notify_20.userInfo = [NSDictionary dictionaryWithObjectsAndKeys:
                                      [[NSString alloc] initWithFormat:@"%d",(int)d_hour], @"hour",
                                      [[NSString alloc] initWithFormat:@"%d",(int)d_minute],@"minute",
                                      [[NSString alloc] initWithFormat:@"%d",(int)20],@"sec",
                                      @"YES", @"ok",nil];
                [[UIApplication sharedApplication] scheduleLocalNotification:notify_20];

                [componentsForFireDate setSecond:40];
                
                // 分ごとに
                notify_40.repeatInterval = NSCalendarUnitMinute;
                notify_40.alertBody = NSLocalizedString(@"timesup",nil);  //アラートのメッセージ本文
                notify_40.alertAction = NSLocalizedString(@"godmor2",nil);  //アクションボタンに表示する文字
                notify_40.soundName = @"normal_alarm_middle.caf";  //アラート表示時に鳴らすサウンドファイル
                notify_40.fireDate = [new_calendar dateFromComponents:componentsForFireDate];
                notify_40.timeZone = [NSTimeZone systemTimeZone];
                notify_40.userInfo = [NSDictionary dictionaryWithObjectsAndKeys:
                                   [[NSString alloc] initWithFormat:@"%d",(int)d_hour], @"hour",
                                   [[NSString alloc] initWithFormat:@"%d",(int)d_minute],@"minute",
                                      [[NSString alloc] initWithFormat:@"%d",(int)40],@"sec",
                                   @"YES", @"ok",nil];
                [[UIApplication sharedApplication] scheduleLocalNotification:notify_40];

            //明日に設定
            }else{
                NSCalendar *new_calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
                NSDate *new_now = [NSDate date];
                NSDateComponents *componentsForFireDate = [new_calendar components:(NSCalendarUnitYear | NSCalendarUnitWeekday | NSCalendarUnitHour | NSCalendarUnitMinute| NSCalendarUnitSecond | NSCalendarUnitWeekday) fromDate: new_now];
                // 時間帯を固定
                [componentsForFireDate setYear: nextyear];
                [componentsForFireDate setMonth: nextmonth];
                [componentsForFireDate setDay: nextday];
                [componentsForFireDate setHour: d_hour];
                [componentsForFireDate setMinute:d_minute];
                [componentsForFireDate setSecond:1];
                [componentsForFireDate setTimeZone:[NSTimeZone systemTimeZone]];
                
                // 分ごとに
                notify.repeatInterval = NSCalendarUnitMinute;
                notify.alertBody = NSLocalizedString(@"timesup",nil);  //アラートのメッセージ本文
                notify.alertAction = NSLocalizedString(@"godmor2",nil);  //アクションボタンに表示する文字
                notify.soundName = @"normal_alarm_middle.caf";  //アラート表示時に鳴らすサウンドファイル
                notify.fireDate = [new_calendar dateFromComponents:componentsForFireDate];
                notify.timeZone = [NSTimeZone systemTimeZone];
                notify.userInfo = [NSDictionary dictionaryWithObjectsAndKeys:
                                   [[NSString alloc] initWithFormat:@"%d",(int)d_hour], @"hour",
                                   [[NSString alloc] initWithFormat:@"%d",(int)d_minute],@"minute",
                                   [[NSString alloc] initWithFormat:@"%d",(int)1],@"sec",
                                   @"YES", @"ok",nil];
                [[UIApplication sharedApplication] scheduleLocalNotification:notify];
                
                [componentsForFireDate setSecond:20];
                // 分ごとに
                notify_20.repeatInterval = NSCalendarUnitMinute;
                notify_20.alertBody = NSLocalizedString(@"timesup",nil);  //アラートのメッセージ本文
                notify_20.alertAction = NSLocalizedString(@"godmor2",nil);  //アクションボタンに表示する文字
                notify_20.soundName = @"normal_alarm_middle.caf";  //アラート表示時に鳴らすサウンドファイル
                notify_20.fireDate = [new_calendar dateFromComponents:componentsForFireDate];
                notify_20.timeZone = [NSTimeZone systemTimeZone];
                notify_20.userInfo = [NSDictionary dictionaryWithObjectsAndKeys:
                                   [[NSString alloc] initWithFormat:@"%d",(int)d_hour], @"hour",
                                   [[NSString alloc] initWithFormat:@"%d",(int)d_minute],@"minute",
                                   [[NSString alloc] initWithFormat:@"%d",(int)20],@"sec",
                                   @"YES", @"ok",nil];
                [[UIApplication sharedApplication] scheduleLocalNotification:notify_20];
                
                [componentsForFireDate setSecond:40];
                // 分ごとに
                notify_40.repeatInterval = NSCalendarUnitMinute;
                notify_40.alertBody = NSLocalizedString(@"timesup",nil);  //アラートのメッセージ本文
                notify_40.alertAction = NSLocalizedString(@"godmor2",nil);  //アクションボタンに表示する文字
                notify_40.soundName = @"normal_alarm_middle.caf";  //アラート表示時に鳴らすサウンドファイル
                notify_40.fireDate = [new_calendar dateFromComponents:componentsForFireDate];
                notify_40.timeZone = [NSTimeZone systemTimeZone];
                notify_40.userInfo = [NSDictionary dictionaryWithObjectsAndKeys:
                                      [[NSString alloc] initWithFormat:@"%d",(int)d_hour], @"hour",
                                      [[NSString alloc] initWithFormat:@"%d",(int)d_minute],@"minute",
                                      [[NSString alloc] initWithFormat:@"%d",(int)40],@"sec",
                                      @"YES", @"ok",nil];
                [[UIApplication sharedApplication] scheduleLocalNotification:notify_40];

            }
        }
        
        
        for(int h=0; h<=8; h++) {
            NSDateComponents *comps;
            NSUInteger flags;
            
            NSDateComponents *n_comps = [[NSDateComponents alloc]init];
            // 1日後とする
            [n_comps setDay:h];
            
            // 1日後のNSDateインスタンスを取得する
            NSDate *date = [[NSCalendar currentCalendar] dateByAddingComponents:n_comps toDate:[NSDate date] options:0];
            flags = NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay;
            comps = [calendar components:flags fromDate:date];
            
            NSInteger year = comps.year;
            NSInteger month = comps.month;
            NSInteger day = comps.day;
        
            // 時・分・秒を取得
            flags = NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond;
            comps = [calendar components:flags fromDate:date];
        
            // 曜日
            comps = [calendar components:NSCalendarUnitWeekday fromDate:date];
            NSInteger weekday = comps.weekday; // 曜日(1が日曜日 7が土曜日)
            weekday = weekday - 1;
            // 上記リクエストを元に件数を取得します。
            for (int i = 0; i < result.count; i++) {
                UILocalNotification *notify2 = [[UILocalNotification alloc] init];
                UILocalNotification *notify2_20 = [[UILocalNotification alloc] init];
                UILocalNotification *notify2_40 = [[UILocalNotification alloc] init];
                
                id obj = [result objectAtIndex: i];
                NSInteger d_hour = [[obj valueForKey:@"hour"] intValue];
                NSInteger d_minute = [[obj valueForKey:@"minute"] intValue];

                int sun = [[obj valueForKey:@"sun"] intValue];
                int mon = [[obj valueForKey:@"mon"] intValue];
                int tue = [[obj valueForKey:@"tue"] intValue];
                int wed = [[obj valueForKey:@"wed"] intValue];
                int thu = [[obj valueForKey:@"thu"] intValue];
                int fri = [[obj valueForKey:@"fri"] intValue];
                int sat = [[obj valueForKey:@"sat"] intValue];
            
                NSMutableArray *week_array = [NSMutableArray array];
                [week_array addObject:[NSNumber numberWithInt:sun]];
                [week_array addObject:[NSNumber numberWithInt:mon]];
                [week_array addObject:[NSNumber numberWithInt:tue]];
                [week_array addObject:[NSNumber numberWithInt:wed]];
                [week_array addObject:[NSNumber numberWithInt:thu]];
                [week_array addObject:[NSNumber numberWithInt:fri]];
                [week_array addObject:[NSNumber numberWithInt:sat]];
            
                int res = [[week_array objectAtIndex:weekday ] intValue];
                if(res == 1){
                    NSCalendar *new_calendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
                    NSDate *new_now = [NSDate date];
                    NSDateComponents *componentsForFireDate = [new_calendar components:(NSCalendarUnitYear | NSCalendarUnitWeekday | NSCalendarUnitHour | NSCalendarUnitMinute| NSCalendarUnitSecond | NSCalendarUnitWeekday) fromDate: new_now];
                    // 時間帯を固定
                    [componentsForFireDate setYear: year];
                    [componentsForFireDate setMonth: month];
                    [componentsForFireDate setDay: day];
                    [componentsForFireDate setHour: d_hour];
                    [componentsForFireDate setMinute:d_minute];
                    [componentsForFireDate setWeekday: weekday];
                    [componentsForFireDate setSecond:0];
                    [componentsForFireDate setTimeZone:[NSTimeZone systemTimeZone]];
            
                    NSLog(@"date: %d", day);
                    NSLog(@"date: %d", weekday);
                    NSDate * notification_date = [calendar dateFromComponents:componentsForFireDate];
                    NSLog(@"date: %@", notification_date);
                    
                    // 一日ごとに通知を送る
                    notify2.repeatInterval = NSCalendarUnitMinute;
                    notify2.alertBody = NSLocalizedString(@"timesup",nil);  //アラートのメッセージ本文
                    notify2.alertAction = NSLocalizedString(@"godmor2",nil);  //アクションボタンに表示する文字
                    notify2.soundName = @"normal_alarm_middle.caf";  //アラート表示時に鳴らすサウンドファイル
                    notify2.fireDate = [new_calendar dateFromComponents:componentsForFireDate];
                    notify2.timeZone = [NSTimeZone systemTimeZone];
                    NSComparisonResult result = [notify2.fireDate compare:now];

                    switch(result) {
                        case NSOrderedSame: // 一致したとき
                            break;
                            
                        case NSOrderedAscending:
                            break;
                            
                        case NSOrderedDescending:
                            [componentsForFireDate setSecond:20];
                            // 一日ごとに通知を送る
                            notify2_20.repeatInterval = NSCalendarUnitMinute;
                            notify2_20.alertBody = NSLocalizedString(@"timesup",nil);  //アラートのメッセージ本文
                            notify2_20.alertAction = NSLocalizedString(@"godmor2",nil);  //アクションボタンに表示する文字
                            notify2_20.soundName = @"normal_alarm_middle.caf";  //アラート表示時に鳴らすサウンドファイル
                            notify2_20.fireDate = [new_calendar dateFromComponents:componentsForFireDate];
                            notify2_20.timeZone = [NSTimeZone systemTimeZone];
                            notify2_20.userInfo = [NSDictionary dictionaryWithObjectsAndKeys:
                                                  [[NSString alloc] initWithFormat:@"%d",(int)d_hour], @"hour",
                                                  [[NSString alloc] initWithFormat:@"%d",(int)d_minute],@"minute",
                                                  [[NSString alloc] initWithFormat:@"%d",(int)20],@"sec",
                                                  @"YES", @"ok",nil];
                            
                            [componentsForFireDate setSecond:40];
                            // 一日ごとに通知を送る
                            notify2_40.repeatInterval = NSCalendarUnitMinute;
                            notify2_40.alertBody = NSLocalizedString(@"timesup",nil);  //アラートのメッセージ本文
                            notify2_40.alertAction = NSLocalizedString(@"godmor2",nil);  //アクションボタンに表示する文字
                            notify2_40.soundName = @"normal_alarm_middle.caf";  //アラート表示時に鳴らすサウンドファイル
                            notify2_40.fireDate = [new_calendar dateFromComponents:componentsForFireDate];
                            notify2_40.timeZone = [NSTimeZone systemTimeZone];
                            notify2_40.userInfo = [NSDictionary dictionaryWithObjectsAndKeys:
                                                   [[NSString alloc] initWithFormat:@"%d",(int)d_hour], @"hour",
                                                   [[NSString alloc] initWithFormat:@"%d",(int)d_minute],@"minute",
                                                   [[NSString alloc] initWithFormat:@"%d",(int)40],@"sec",
                                                   @"YES", @"ok",nil];
                            
                            [[UIApplication sharedApplication] scheduleLocalNotification:notify2];
                            [[UIApplication sharedApplication] scheduleLocalNotification:notify2_20];
                            [[UIApplication sharedApplication] scheduleLocalNotification:notify2_40];
                            break;
                    }
//                    if(h == 0 && (d_hour <= hour && d_minute <= minute )){
//                        [[UIApplication sharedApplication] cancelLocalNotification:notify2];
//                    }
                }
            }
        }
//    for (UILocalNotification *notify in [[UIApplication sharedApplication] scheduledLocalNotifications]) {
//        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
//        [formatter setDateFormat:@"yyyy/MM/dd HH:mm:ss"];
//        NSString *dateStr = [formatter stringFromDate:notify.fireDate];
//        NSLog(@"%@", dateStr);
//    }
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    printf("%@", on_timer);
    if(on_timer == true){
        NSDate *now = [NSDate date];
        NSTimeInterval delta = [notification.fireDate timeIntervalSinceDate:now];
        if(delta >= 0){
            time = (int)delta;
        } else{
            time = 0;
        }
    }
    
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];

    NSEntityDescription *my_entity
    = [NSEntityDescription entityForName:@"Myschedule" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:my_entity];
    
    // ソート条件を指定します。
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"createdat" ascending:NO];
    NSArray *my_sortDescriptors = @[sortDescriptor];
    [fetchRequest setSortDescriptors:my_sortDescriptors];
    NSArray *my_result= [self.managedObjectContext executeFetchRequest:fetchRequest error:nil];
    [self.itemArray removeAllObjects];
    for (int i = 0; i < my_result.count; i++) {
        id obj = [my_result objectAtIndex: i];
        [self.itemArray addObject:[obj valueForKey:@"minute"]];
    }
//    NSLog(@"%@", self.itemArray);

    NSDate *now = [NSDate date];
    
    NSDateComponents *n_comps = [[NSDateComponents alloc]init];
    // 1日後とする
    [n_comps setMinute:-60];
    
    // 1日後のNSDateインスタンスを取得する
    NSDate *n_date = [[NSCalendar currentCalendar] dateByAddingComponents:n_comps toDate:[NSDate date] options:0];
    
//    NSDateComponents *n_comps = [[NSDateComponents alloc]init];

    int setOff = 0;
    for (UILocalNotification *notify in [[UIApplication sharedApplication] scheduledLocalNotifications]) {
        bool before = NO;
        NSComparisonResult result1 = [notify.fireDate compare:now];
        switch(result1) {
            case NSOrderedSame: // 一致したとき
                before = YES;
                break;
                
            case NSOrderedAscending: // date1が小さいとき
                before = YES;
                break;

            case NSOrderedDescending: // date1が大きいとき
                break;
        }
        if(before == YES){
            NSComparisonResult result2 = [n_date compare:notify.fireDate];
            switch(result2) {
                case NSOrderedSame: // 一致したとき
                    on_alarm = YES;
                    [[UIApplication sharedApplication] cancelLocalNotification:notify];
                    break;
                    
                case NSOrderedAscending: // date1が小さいとき
                    on_alarm = YES;
                    [[UIApplication sharedApplication] cancelLocalNotification:notify];
                    break;
                
                case NSOrderedDescending: // date1が大きいとき
                    break;
            }
        }   
        NSString *hour = [notify.userInfo objectForKey:@"hour"];
        NSString *minute = [notify.userInfo objectForKey:@"minute"];

        //タイマーをセットする
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        
        // 対象エンティティを指定します。
        NSEntityDescription *entity
        = [NSEntityDescription entityForName:@"Alarm" inManagedObjectContext:self.managedObjectContext];
        [fetchRequest setEntity:entity];
        
        // 検索条件を指定します。
        NSPredicate *pred
        = [NSPredicate predicateWithFormat:@"on = YES and hour= %@ and minute = %@", hour, minute];
        [fetchRequest setPredicate:pred];
        // ソート条件を指定します。
        NSSortDescriptor *hourDisc = [[NSSortDescriptor alloc] initWithKey:@"hour" ascending:YES];
        NSSortDescriptor *minuteDisc = [[NSSortDescriptor alloc] initWithKey:@"minute" ascending:YES];
        NSArray *sortDescriptors = [NSArray arrayWithObjects:hourDisc,minuteDisc, nil];
        [fetchRequest setSortDescriptors:sortDescriptors];
        NSArray *result= [self.managedObjectContext executeFetchRequest:fetchRequest error:nil];
        
        for (int i = 0; i < result.count; i++) {
            id obj = [result objectAtIndex: i];
            
            int sun = [[obj valueForKey:@"sun"] intValue];
            int mon = [[obj valueForKey:@"mon"] intValue];
            int tue = [[obj valueForKey:@"tue"] intValue];
            int wed = [[obj valueForKey:@"wed"] intValue];
            int thu = [[obj valueForKey:@"thu"] intValue];
            int fri = [[obj valueForKey:@"fri"] intValue];
            int sat = [[obj valueForKey:@"sat"] intValue];
            
            if(sun == 0 && mon == 0 && tue == 0 && wed == 0 && thu == 0 && fri == 0 && sat ==0 && on_alarm == YES  && setOff == 0){
                setOff = 1;
                [obj setValue:[NSNumber numberWithBool:NO] forKey:@"on"];
                NSError *error = nil;
                if (![self.managedObjectContext save:&error]) {
                    // Replace this implementation with code to handle the error appropriately.
                    // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
                    abort();
                }else{
                    NSLog(@"appdelegateにてオフにしました。");
                }
            }
        }
        break;
    }
    
    if(on_alarm == YES){
        //    if (application.applicationState == UIApplicationStateInactive) {
        //パターン３：アプリがバックグラウンドでは生きている時に通知をタップ
        UITabBarController *tbc = (UITabBarController *)self.window.rootViewController;
        UINavigationController *navigationController = tbc.viewControllers[0];
        NSString* viewIdentifier = @"moveView";
        UIStoryboard* sb = [[[self window] rootViewController] storyboard];
        UIViewController* vc = [[UIViewController alloc] init];
        vc = [sb instantiateViewControllerWithIdentifier:viewIdentifier];
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
        [self.window makeKeyAndVisible];
 //       self.percent = 0;
        self.sub = 0;
        [navigationController presentViewController:nav animated:NO completion:nil];
    }

    [[UIApplication sharedApplication] cancelAllLocalNotifications];
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Saves changes in the application's managed object context before the application terminates.
    [self saveContext];
}

- (void)saveContext
{
    NSError *error = nil;
    NSManagedObjectContext *managedObjectContext = self.managedObjectContext;
    if (managedObjectContext != nil) {
        if ([managedObjectContext hasChanges] && ![managedObjectContext save:&error]) {
             // Replace this implementation with code to handle the error appropriately.
             // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            abort();
        } 
    }
}

#pragma mark - Core Data stack

// Returns the managed object context for the application.
// If the context doesn't already exist, it is created and bound to the persistent store coordinator for the application.
- (NSManagedObjectContext *)managedObjectContext
{
    if (_managedObjectContext != nil) {
        return _managedObjectContext;
    }
    
    NSPersistentStoreCoordinator *coordinator = [self persistentStoreCoordinator];
    if (coordinator != nil) {
        _managedObjectContext = [[NSManagedObjectContext alloc] init];
        [_managedObjectContext setPersistentStoreCoordinator:coordinator];
    }
    return _managedObjectContext;
}

// Returns the managed object model for the application.
// If the model doesn't already exist, it is created from the application's model.
- (NSManagedObjectModel *)managedObjectModel
{
    if (_managedObjectModel != nil) {
        return _managedObjectModel;
    }
    NSURL *modelURL = [[NSBundle mainBundle] URLForResource:@"Naptimer" withExtension:@"momd"];
    _managedObjectModel = [[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL];
    return _managedObjectModel;
}

// Returns the persistent store coordinator for the application.
// If the coordinator doesn't already exist, it is created and the application's store added to it.
- (NSPersistentStoreCoordinator *)persistentStoreCoordinator
{
    if (_persistentStoreCoordinator != nil) {
        return _persistentStoreCoordinator;
    }
    
    NSURL *storeURL = [[self applicationDocumentsDirectory] URLByAppendingPathComponent:@"Naptimer.sqlite"];
    
    NSError *error = nil;
    _persistentStoreCoordinator = [[NSPersistentStoreCoordinator alloc] initWithManagedObjectModel:[self managedObjectModel]];
    if (![_persistentStoreCoordinator addPersistentStoreWithType:NSSQLiteStoreType configuration:nil URL:storeURL options:nil error:&error]) {
        /*
         Replace this implementation with code to handle the error appropriately.
         
         abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development. 
         
         Typical reasons for an error here include:
         * The persistent store is not accessible;
         * The schema for the persistent store is incompatible with current managed object model.
         Check the error message to determine what the actual problem was.
         
         
         If the persistent store is not accessible, there is typically something wrong with the file path. Often, a file URL is pointing into the application's resources directory instead of a writeable directory.
         
         If you encounter schema incompatibility errors during development, you can reduce their frequency by:
         * Simply deleting the existing store:
         [[NSFileManager defaultManager] removeItemAtURL:storeURL error:nil]
         
         * Performing automatic lightweight migration by passing the following dictionary as the options parameter:
         @{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES}
         
         Lightweight migration will only work for a limited set of schema changes; consult "Core Data Model Versioning and Data Migration Programming Guide" for details.
         
         */
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }    
    
    return _persistentStoreCoordinator;
}

#pragma mark - Application's Documents directory

// Returns the URL to the application's Documents directory.
- (NSURL *)applicationDocumentsDirectory
{
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}
- (void)application:(UIApplication *)application
didReceiveLocalNotification:(UILocalNotification *)localnotification
{
//    //    if (application.applicationState == UIApplicationStateInactive) {
////    if (application.applicationState == UIApplicationStateActive) {
//        //パターン２：画面が既に表示されていて通知が飛んできた時に勝手に呼ばれる
//  //      return
// //   }
//    NSString *hour = [localnotification.userInfo objectForKey:@"hour"];
//    NSString *minute = [localnotification.userInfo objectForKey:@"minute"];
//
//    //タイマーをセットする
//    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
//
//    // 対象エンティティを指定します。
//    NSEntityDescription *entity
//    = [NSEntityDescription entityForName:@"Alarm" inManagedObjectContext:self.managedObjectContext];
//    [fetchRequest setEntity:entity];
//
//    // 検索条件を指定します。
//    NSPredicate *pred
//    = [NSPredicate predicateWithFormat:@"on = YES and hour= %@ and minute = %@", hour, minute];
//    [fetchRequest setPredicate:pred];
//    // ソート条件を指定します。
//    NSSortDescriptor *hourDisc = [[NSSortDescriptor alloc] initWithKey:@"hour" ascending:YES];
//    NSSortDescriptor *minuteDisc = [[NSSortDescriptor alloc] initWithKey:@"minute" ascending:YES];
//    NSArray *sortDescriptors = [NSArray arrayWithObjects:hourDisc,minuteDisc, nil];
//    [fetchRequest setSortDescriptors:sortDescriptors];
//    NSArray *result= [self.managedObjectContext executeFetchRequest:fetchRequest error:nil];
//
//    for (int i = 0; i < result.count; i++) {
//        id obj = [result objectAtIndex: i];
//
//        int sun = [[obj valueForKey:@"sun"] intValue];
//        int mon = [[obj valueForKey:@"mon"] intValue];
//        int tue = [[obj valueForKey:@"tue"] intValue];
//        int wed = [[obj valueForKey:@"wed"] intValue];
//        int thu = [[obj valueForKey:@"thu"] intValue];
//        int fri = [[obj valueForKey:@"fri"] intValue];
//        int sat = [[obj valueForKey:@"sat"] intValue];
//
//        if(sun == 0 && mon == 0 && tue == 0 && wed == 0 && thu == 0 && fri == 0 && sat ==0){
//            [obj setValue:[NSNumber numberWithBool:NO] forKey:@"on"];
//            NSError *error = nil;
//            if (![self.managedObjectContext save:&error]) {
//            // Replace this implementation with code to handle the error appropriately.
//            // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
//                NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
//                abort();
//            }else{
//                NSLog(@"appdelegateにてオフにしました。");
//            }
//        }
//        on_alarm = YES;
//    }
//    if(on_alarm == YES){
//        //    if (application.applicationState == UIApplicationStateInactive) {
//        //パターン３：アプリがバックグラウンドでは生きている時に通知をタップ
//        UITabBarController *tbc = (UITabBarController *)self.window.rootViewController;
//        UINavigationController *navigationController = tbc.viewControllers[0];
//        NSString* viewIdentifier = @"moveView";
//        UIStoryboard* sb = [[[self window] rootViewController] storyboard];
//        UIViewController* vc = [[UIViewController alloc] init];
//        vc = [sb instantiateViewControllerWithIdentifier:viewIdentifier];
//        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
//        [self.window makeKeyAndVisible];
//        [navigationController presentViewController:nav animated:NO completion:nil];
//    }
    return;
//    //    }

}
@end
