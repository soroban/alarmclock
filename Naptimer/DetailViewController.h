//
//  DetailViewController.h
//  Naptimer
//
//  Created by 大口 尚紀(管理者) on 2014/05/07.
//  Copyright (c) 2014年 大口 尚紀(管理者). All rights reserved.
//

#import <UIKit/UIKit.h>
#include <AVFoundation/AVFoundation.h>
#import <MediaPlayer/MediaPlayer.h>

@interface DetailViewController : UIViewController
{
    NSTimer* countdownTimer;
    float initialVolume;
    AVAudioSession *session;

}
@property NSString *receiveString;
@property (strong, nonatomic) id detailItem;
@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *countLabel;
@property (weak, nonatomic) IBOutlet UIButton *startButton;
@property  AVAudioPlayer *player;
- (IBAction)showAlert:(id)sender;
- (IBAction)updatePieChart;
- (void)setVolumeNotification; 
//@property (weak, nonatomic) IBOutlet UILabel *countLabel;
@end
