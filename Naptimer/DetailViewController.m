//
//  DetailViewController.m
//  Naptimer
//
//  Created by 大口 尚紀(管理者) on 2014/05/07.
//  Copyright (c) 2014年 大口 尚紀(管理者). All rights reserved.
//

#import "DetailViewController.h"
#import "CompleteViewController.h"
#import "AppDelegate.h"
#import <CoreMotion/CoreMotion.h>
#import <AudioToolbox/AudioToolbox.h>

#define UIColorHex(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

@interface DetailViewController ()
- (void)configureView;
@property(nonatomic, strong) CMMotionManager *motionManager;
@end

@implementation DetailViewController{
    NSInteger _buttonIndex;
    UIView *chart;
    NSTimer *tm;
    double percent;
    double sub;
    double no_update_count;
    AppDelegate *appDelegete;
    float threshold;
    UILabel *percentLabel;
    UILabel *explainLabel;
    int update_time;
    int pre_update_time;
    int not_update_time;
    int pre_not_update_time;
    
}
@synthesize motionManager;

#pragma mark - Managing the detail item

- (void)setDetailItem:(id)newDetailItem
{
    if (_detailItem != newDetailItem) {
        _detailItem = newDetailItem;
        
        // Update the view.
        [self configureView];
    }
}

- (void)configureView
{
    // Update the user interface for the detail item.
    
    if (self.detailItem) {
        self.detailDescriptionLabel.text = [[self.detailItem valueForKey:@"timeStamp"] description];
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    AVAudioSession *session = [AVAudioSession sharedInstance];
    [session setCategory:AVAudioSessionCategoryPlayback error:NULL];
    [session setActive:YES error:NULL];
    
    //以下追加
    pre_update_time = [[NSDate date] timeIntervalSince1970];
    not_update_time = [[NSDate date] timeIntervalSince1970];
    pre_not_update_time = [[NSDate date] timeIntervalSince1970];
    no_update_count = 0;
    
    //  ナビゲーションバーのタイトルの色を白色に変更する
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectZero];
    titleLabel.font = [UIFont fontWithName:@"Helvetica" size:15];
    titleLabel.text = NSLocalizedString(@"timer",nil);
    titleLabel.textColor = [UIColor grayColor];
    [titleLabel sizeToFit];
    self.navigationItem.titleView = titleLabel;
    
    appDelegete = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    
    appDelegete.on_alarm = false;
    appDelegete.on_timer = false;
    if(appDelegete.on_timer == false && appDelegete.on_alarm == false){
        appDelegete.time = [self.receiveString intValue]*60;
    }
    self.countLabel.text = [NSString stringWithFormat:@"%02d:%02d:%02d",appDelegete.time/(60*60), (appDelegete.time/60)%60,00];
	// Do any additional setup after loading the view, typically from a nib.
    //[self updatePieChart];
    [self configureView];
}
- (void)setVolumeNotification {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(volumeChanged:) name:@"AVSystemController_SystemVolumeDidChangeNotification" object:nil];
}

- (void)volumeChanged:(NSNotification *)notification{
    //明示的にボリューム変更がされた時のみ
    if ([[[notification userInfo] objectForKey:@"AVSystemController_AudioVolumeChangeReasonNotificationParameter"] isEqualToString:@"ExplicitVolumeChange"]) {
        
        //ここに押下処理を記述
        MPMusicPlayerController *playerController = [MPMusicPlayerController systemMusicPlayer];
        [playerController setValue:@(1.0) forKey:@"volume"];
        [playerController setValue:@(1.0) forKey:@"volumePrivate"];
        
        //一旦NSNotificationCenterからAVSystemController_SystemVolumeDidChangeNotificationを外して、ボリュームを元に戻す
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"AVSystemController_SystemVolumeDidChangeNotification" object:nil];
        [self performSelector:@selector(setVolumeNotification) withObject:nil afterDelay:0.2];
    }
}

- (void)applicationDidEnterBackground:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"AVSystemController_SystemVolumeDidChangeNotification" object:nil];
    [session setActive:NO error:nil];
}

- (void)applicationWillEnterForeground:(NSNotification *)notification {
    [self setVolumeNotification];
    [session setActive:YES error:nil];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)countDown
{
    tm = [NSTimer scheduledTimerWithTimeInterval:1.0f
                                          target:self
                                        selector:@selector(countdownTimer:)
                                        userInfo:nil
                                         repeats:YES
          ];
}

- (void)countdownTimer:(NSTimer *)timer
{
    if (appDelegete.time <= 0.0) {
        [tm invalidate];
//        [self getLight];
        self.countLabel.hidden = true;
        appDelegete.on_timer = false;
        appDelegete.on_alarm = true;
        [self goMoveView];
        //[self updatePieChart];
    }
    else {
        NSLog(@"%d",appDelegete.time);
        appDelegete.time--;
        self.countLabel.text = [NSString stringWithFormat:@"%02d:%02d:%02d",appDelegete.time/(60*60), (appDelegete.time/60)%60,appDelegete.time%60];
    }
}


- (IBAction)showAlert:(id)sender{
    UIAlertView *alert = [[UIAlertView alloc]
                          initWithTitle:NSLocalizedString(@"conf",nil)
                          message:NSLocalizedString(@"run",nil)
                          delegate:self
                          cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
    [alert show];
}

/**
 * アラートのボタンが押されたとき
 */
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    _buttonIndex =buttonIndex;
    switch (buttonIndex) {
        case 1: // Button1が押されたとき
        {
            [self countDown];
            
            appDelegete.on_timer = true;
            self.startButton.hidden = true;
            UIView* tempView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 10, 10)];
            UIBarButtonItem* tempButton = [[UIBarButtonItem alloc] initWithCustomView:tempView];
            tempButton.enabled = NO;
            self.navigationItem.leftBarButtonItem = tempButton;
            break;
        }
        default: // キャンセルが押されたとき
        {
            [self.navigationController popViewControllerAnimated:YES];
            [countdownTimer invalidate];
            break;
        }
    }
}

- (void)goMoveView
{
    //    [self performSegueWithIdentifier:@"Move" sender:self];
    //    MoveViewController *newVC = [self.storyboard instantiateViewControllerWithIdentifier:@"moveView"];
    //    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:newVC];
    //    [self.navigationController presentViewController:nav animated:NO completion:nil];
    
    //    MoveViewController *newVC = [self.storyboard instantiateViewControllerWithIdentifier:@"moveView"];
    //    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:newVC];
    //    [[[[UIApplication sharedApplication] delegate] window] setRootViewController:nav];
    
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    UIViewController *rootViewController = window.rootViewController;
    [[[[UIApplication sharedApplication] delegate] window] setRootViewController:rootViewController];
    UITabBarController *tbc = (UITabBarController *)window.rootViewController;
    UINavigationController *navigationController = tbc.viewControllers[0];
    
    NSString* viewIdentifier = @"moveView";
    UIStoryboard* sb = self.storyboard;
    UIViewController* vc = [[UIViewController alloc] init];
    vc = [sb instantiateViewControllerWithIdentifier:viewIdentifier];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
    [navigationController presentViewController:nav animated:NO completion:nil];
}

- (void)getLight
{
    [UIApplication sharedApplication].idleTimerDisabled = YES;
    threshold = 0.5;
    
    CGRect rect1 = [[UIScreen mainScreen] bounds];
    // UIImageViewの初期化
    CGRect rect_taisou = CGRectMake(10, rect1.size.height-(rect1.size.height/3.8), 130, 130);
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:rect_taisou];
    
    // 画像の読み込み
    imageView.image = [UIImage imageNamed:@"taisou.png"];
    
    // UIImageViewのインスタンスをビューに追加
    [self.view addSubview:imageView];
    
    
    CGRect rect_aruku = CGRectMake(150, rect1.size.height-(rect1.size.height/3.8), 130, 130);
    imageView = [[UIImageView alloc]initWithFrame:rect_aruku];
    
    // 画像の読み込み
    imageView.image = [UIImage imageNamed:@"aruku.png"];
    
    // UIImageViewのインスタンスをビューに追加
    [self.view addSubview:imageView];
    
    [self startPieChart];
    [self playSound];
    
    
    // 加速度データの更新間隔を0.1秒ごとに設定
    motionManager = [[CMMotionManager alloc] init];
    // 加速度データの更新間隔を0.1秒ごとに設定
    motionManager.accelerometerUpdateInterval = 0.5;
    
    //    __block double change_date = 0.0;
    // 加速度センサが利用可能かチェック
    if (motionManager.accelerometerAvailable)
    {
        // モーションデータ更新時のハンドラを作成
        void (^handler)(CMDeviceMotion *, NSError *) = ^(CMDeviceMotion *motion, NSError *error)
        {
            // ユーザー加速度の重力方向の大きさを算出
            double magnitude = [self gravityDirectionMagnitudeForMotion:motion];
            if(threshold < fabs(magnitude)){
                [self updatePieChart];
                no_update_count = 0;
            }else{
                pre_not_update_time = not_update_time;
                not_update_time = [[NSDate date] timeIntervalSince1970];
            }
            
            if((not_update_time-pre_not_update_time) == 0){
                no_update_count++;
            }
            if(no_update_count > 100){
                if(self.player.volume <= 1.0){
                    self.player.volume = self.player.volume + 0.1;
                }
                no_update_count = 0;
            }
            
            //停止
            if(percent > 1.01){
                [motionManager stopDeviceMotionUpdates];
                [self stopTimer];
            }
        };
        
        // モーションデータの測定を開始
        NSOperationQueue *queue = [NSOperationQueue currentQueue];
        [motionManager startDeviceMotionUpdatesToQueue:queue withHandler:handler];
    }
}

- (void)startPieChart
{
    //アニメーションの対象となるコンテキスト
    CGContextRef context = UIGraphicsGetCurrentContext();
    [UIView beginAnimations:nil context:context];
    //アニメーションを実行する時間
    [UIView setAnimationDuration:1.0];
    //アニメーションイベントを受け取るview
    [UIView setAnimationDelegate:self.view];
    
    chart = [[UIView alloc] initWithFrame:CGRectMake(60, 90, 200, 200)];
    chart.backgroundColor = [UIColor grayColor];
    chart.layer.cornerRadius = 100;
    chart.alpha = 0.0;
    [self.view addSubview:chart];
    UIBezierPath *path = path = [UIBezierPath bezierPath];
    [path moveToPoint:CGPointMake(100, 100)];
    [path addArcWithCenter:CGPointMake(100, 100) radius:100 startAngle:-0.5*M_PI endAngle:-0.5*M_PI + ((2*M_PI)*0) clockwise:YES];
    CAShapeLayer *sl;
    sl = [[CAShapeLayer alloc] init];
    sl.fillColor = [self color:1].CGColor;
    sl.path = path.CGPath;
    [chart.layer addSublayer:sl];
    [chart setAlpha:1.0];
    [UIView commitAnimations];
    percent = 0;
    
    percentLabel = [[UILabel alloc] init];
    percentLabel.frame = CGRectMake(102, 167, 120, 50);
    percentLabel.font = [UIFont fontWithName:@"Helvetica" size:23];
    percentLabel.textColor = [UIColor grayColor];
    percentLabel.textAlignment = NSTextAlignmentCenter;
    percentLabel.text = @"0.00%";
    [self.view addSubview:percentLabel];
    
    UIView *mask = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 120, 120)];
    mask.layer.cornerRadius = 60;
    mask.center = CGPointMake(100, 100);
    mask.backgroundColor = [UIColor whiteColor];
    [chart addSubview:mask];
    
    UILabel *meterLabel = [[UILabel alloc] init];
    meterLabel.frame = CGRectMake(102, 280, 120, 50);
    meterLabel.font = [UIFont fontWithName:@"Helvetica" size:20];
    meterLabel.textColor = [UIColor grayColor];
    meterLabel.textAlignment = NSTextAlignmentCenter;
    meterLabel.text = @"move meter";
    [self.view addSubview:meterLabel];
    
    explainLabel = [[UILabel alloc] init];
    CGRect rect1 = [[UIScreen mainScreen] bounds];
    explainLabel.frame = CGRectMake(10, rect1.size.height-(rect1.size.height/1.3), rect1.size.width-20, 500);
    explainLabel.font = [UIFont fontWithName:@"Helvetica" size:14];
    explainLabel.textColor = [UIColor grayColor];
    explainLabel.textAlignment = NSTextAlignmentLeft;
    explainLabel.numberOfLines = 0;
    explainLabel.text = NSLocalizedString(@"explain",nil);
    [self.view addSubview:explainLabel];
    
}

- (void)updatePieChart
{
    pre_update_time = [[NSDate date] timeIntervalSince1970];
    not_update_time = [[NSDate date] timeIntervalSince1970];
    pre_not_update_time = [[NSDate date] timeIntervalSince1970];
    no_update_count = 0;

    percent = percent + 0.00009;
    sub = sub + 0.00009;
//        percent = percent +0.1;
//        sub = sub + 0.1;
//        percent = 1.1;    
    
    NSString *text = percentLabel.text;
    if([text isEqualToString:[NSString stringWithFormat:@"%04.1f%%",percent*100]]) {

    }else{
        percentLabel.text = nil;
        percentLabel.text = [NSString stringWithFormat:@"%04.1f%%",percent*100];
        
        UIBezierPath *path = [UIBezierPath bezierPath];
        [path moveToPoint:CGPointMake(100, 100)];
        CAShapeLayer *sl = [[CAShapeLayer alloc] init];
        if(sub > 0.01){
            if(percent <= 0.5){
                [path addArcWithCenter:CGPointMake(100, 100) radius:100 startAngle:-0.5*M_PI endAngle:-0.5*M_PI + ((2*M_PI)*(percent+0.01)) clockwise:YES];
            }else{
                [path addArcWithCenter:CGPointMake(100, 100) radius:100 startAngle:0.5*M_PI endAngle:0.5*M_PI + ((2*M_PI) * ((percent+0.01)-0.5)) clockwise:YES];
            }
            sl.fillColor = [self color:1].CGColor;
            sl.path = path.CGPath;
            [chart.layer insertSublayer:sl atIndex:0];
            sub =0;
            
            update_time = [[NSDate date] timeIntervalSince1970];
            if((update_time - pre_update_time) <= 1){
                if(self.player.volume >= 0.0){
                    self.player.volume = self.player.volume - 0.1;
                }
            }
            pre_update_time = update_time;
        }
    }
    
    if(percent >= 1.00){
        [self stopTimer];
    }
}

- (double)gravityDirectionMagnitudeForMotion:(CMDeviceMotion *)motion
{
    // ユーザー加速度の測定値を取得
    CMAcceleration user = motion.userAcceleration;
    // 重力加速度の測定値を取得
    //    CMAcceleration gravity = motion.gravity;
    
    // ユーザー加速度の大きさを算出
    double magnitude = sqrt(pow(user.x, 2) + pow(user.y, 2) + pow(user.z, 2));
    
    // ユーザー加速度のベクトルと重力加速度のベクトルのなす角θのcosθを算出
    //    double cosT = (user.x * gravity.x + user.y * gravity.y + user.z * gravity.z) /
    //    sqrt((pow(user.x, 2) + pow(user.y, 2) + pow(user.z, 2)) *
    //         (pow(gravity.x, 2) + pow(gravity.y, 2) + pow(gravity.z, 2)));
    
    // ユーザー加速度の大きさにcosθを乗算してユーザー加速度の重力方向における大きさを算出し、小数点第3位で丸める
    //    double gravityDirectionMagnitude = round(magnitude * cosT * 100) / 100;
    double gravityDirectionMagnitude = round(magnitude * 100) / 100;
    return gravityDirectionMagnitude;
}

- (void)playSound{
    MPMusicPlayerController *playerController = [MPMusicPlayerController systemMusicPlayer];
        [playerController setValue:@(1.0) forKey:@"volume"];
        [playerController setValue:@(1.0) forKey:@"volumePrivate"];

    NSString *soundPath = [[NSBundle mainBundle] pathForResource:@"normal_alarm" ofType:@"m4a"];
    NSURL *url = [NSURL fileURLWithPath:soundPath];
    self.player = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:NULL];
    [self.player setNumberOfLoops:0];
    self.player.volume = 1.0;
    self.player.numberOfLoops = -1;
    self.player.delegate = (id)self;
    [self.player prepareToPlay];
    [self.player play];
}

- (void)stopTimer{
    appDelegete.on_alarm = false;
    [self.player stop];
    CompleteViewController *newVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CompleteView"];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:newVC];
    [self.navigationController presentViewController:nav animated:NO completion:nil];
    
}

- (UIColor*)color:(int)i
{
    switch (i) {
        case 0:
            return UIColorHex(0xF24495);
        case 1:
            return UIColorHex(0x04BFBF);
        case 2:
            return UIColorHex(0xB2F252);
        case 3:
            return UIColorHex(0xF2CB05);
        case 4:
            return UIColorHex(0xE9F2DF);
        default:
            break;
    }
    return nil;
}

- (void)viewDidAppear:(BOOL)animated
{
}

- (void)viewWillAppear:(BOOL)animated
{
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    UITabBarController *tabBarController = (UITabBarController *)window.rootViewController;
    GADBannerView *bannerView = (GADBannerView*)[tabBarController.view viewWithTag:9999999];
    if(bannerView != nil){
        CGRect bannerFrame = bannerView.frame;
        bannerFrame.origin.y = self.view.bounds.size.height - (1 * bannerFrame.size.height);
        bannerView.frame = bannerFrame;
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    UITabBarController *tabBarController = (UITabBarController *)window.rootViewController;
    GADBannerView *bannerView = (GADBannerView*)[tabBarController.view viewWithTag:9999999];
    if(bannerView == nil){
        GADBannerView *bannerView = [[GADBannerView alloc] initWithAdSize:kGADAdSizeLargeBanner];
        bannerView.adSize = kGADAdSizeLargeBanner;
        bannerView.adUnitID = @"ca-app-pub-8256083710740545/8580743719";
        bannerView.tag = 9999999;
        bannerView.rootViewController = tabBarController;
        [tabBarController.view addSubview:bannerView];
        [bannerView loadRequest:[GADRequest request]];
    }
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    bannerView.center = CGPointMake(screenSize.width/2, screenSize.height - tabBarController.tabBar.frame.size.height - (bannerView.frame.size.height / 2)  - 1);
}

@end
