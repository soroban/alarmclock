//
//  FirstViewController.h
//  naptimer
//
//  Created by 大口 尚紀(管理者) on 2014/09/19.
//  Copyright (c) 2014年 大口 尚紀(管理者). All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FirstViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIButton *goMasterBT;
- (IBAction)goMaster:(id)sender;
@end
