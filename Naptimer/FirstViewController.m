//
//  FirstViewController.m
//  naptimer
//
//  Created by 大口 尚紀(管理者) on 2014/09/19.
//  Copyright (c) 2014年 大口 尚紀(管理者). All rights reserved.
//

#import "FirstViewController.h"
#import "MasterViewController.h"
#import "AppDelegate.h"

@interface FirstViewController ()
@property (weak, nonatomic) IBOutlet UITextView *secLabel;
@property (weak, nonatomic) IBOutlet UIImageView *settingImage;

@end

@implementation FirstViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectZero];
    titleLabel.font = [UIFont fontWithName:@"Helvetica" size:15];
    titleLabel.text = NSLocalizedString(@"set",nil);
    titleLabel.textColor = [UIColor grayColor];
    [titleLabel sizeToFit];
    
    NSMutableAttributedString *label;
    label = [[NSMutableAttributedString alloc] initWithString:NSLocalizedString(@"labelSec",nil)];
    [label addAttribute:NSFontAttributeName
                       value:[UIFont fontWithName:nil size:19.]
                       range:NSMakeRange(0, [label length])];
    [self.secLabel setAttributedText:label];
    self.navigationItem.titleView = titleLabel;
    
    
    UIImage *img = [UIImage imageNamed:NSLocalizedString(@"image",nil)];
    [self.settingImage setImage:img];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated
{
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    UITabBarController *tabBarController = (UITabBarController *)window.rootViewController;
    GADBannerView *bannerView = (GADBannerView*)[tabBarController.view viewWithTag:9999999];
    CGRect bannerFrame = bannerView.frame;
    bannerFrame.origin.y = self.view.bounds.size.height - (0 * bannerFrame.size.height);
    bannerView.frame = bannerFrame;
}

- (void)viewWillDisappear:(BOOL)animated
{
//    UIWindow *window = [UIApplication sharedApplication].keyWindow;
//    UITabBarController *tabBarController = (UITabBarController *)window.rootViewController;
//    GADBannerView *bannerView = (GADBannerView*)[tabBarController.view viewWithTag:9999999];
//    CGRect bannerFrame = CGRectMake(0.0,
//               tabBarController.view.frame.size.height -
//               GAD_SIZE_320x50.height - 49,
//               GAD_SIZE_320x50.width, GAD_SIZE_320x50.height);
//    bannerView.frame = bannerFrame;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)goMaster:(id)sender {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    id firstViewController = [storyboard instantiateInitialViewController];
    AppDelegate *appDelegete = [[UIApplication sharedApplication] delegate];
    appDelegete.window.rootViewController = firstViewController;
    [appDelegete.window makeKeyAndVisible];
    
//    [self presentViewController:(UIViewController *)firstViewController animated:NO  completion:nil];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
