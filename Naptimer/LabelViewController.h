//
//  LabelViewController.h
//  Naptimer
//
//  Created by 大口 尚紀(管理者) on 2014/07/23.
//  Copyright (c) 2014年 大口 尚紀(管理者). All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol LabelViewControllerDelegate;
@interface LabelViewController : UIViewController
@property (nonatomic, assign) id<LabelViewControllerDelegate> delegate;
@property (weak, nonatomic) IBOutlet UITextField *alarmLabel;
@property NSString *label_str;
@end

@protocol LabelViewControllerDelegate <NSObject>
//protocolで定義するメソッド
-(void)labelViewController:(LabelViewController *)labelViewController didCloseLabel:(NSString *)label_str;
@end