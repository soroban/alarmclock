//
//  LabelViewController.m
//  Naptimer
//
//  Created by 大口 尚紀(管理者) on 2014/07/23.
//  Copyright (c) 2014年 大口 尚紀(管理者). All rights reserved.
//

#import "LabelViewController.h"
#import "AlarmsetupViewController.h"
#import "AppDelegate.h"

@interface LabelViewController ()

@end

@implementation LabelViewController
@synthesize delegate;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    _alarmLabel.text = self.label_str;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (void)viewWillAppear:(BOOL)animated
{
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    UITabBarController *tabBarController = (UITabBarController *)window.rootViewController;
    GADBannerView *bannerView = (GADBannerView*)[tabBarController.view viewWithTag:9999999];
    if(bannerView != nil){
        CGRect bannerFrame = bannerView.frame;
        bannerFrame.origin.y = self.view.bounds.size.height - (1 * bannerFrame.size.height);
        bannerView.frame = bannerFrame;
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [delegate labelViewController:self didCloseLabel:_alarmLabel.text];
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    UITabBarController *tabBarController = (UITabBarController *)window.rootViewController;
    GADBannerView *bannerView = (GADBannerView*)[tabBarController.view viewWithTag:9999999];
    if(bannerView == nil){
        GADBannerView *bannerView = [[GADBannerView alloc] initWithAdSize:kGADAdSizeLargeBanner];
        bannerView.adSize = kGADAdSizeLargeBanner;
        bannerView.adUnitID = @"ca-app-pub-8256083710740545/8580743719";
        bannerView.tag = 9999999;
        bannerView.rootViewController = tabBarController;
        [tabBarController.view addSubview:bannerView];
        [bannerView loadRequest:[GADRequest request]];
    }
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    bannerView.center = CGPointMake(screenSize.width/2, screenSize.height - tabBarController.tabBar.frame.size.height - (bannerView.frame.size.height / 2)  - 1);
}

- (void)viewDidAppear:(BOOL)animated
{

}

@end
