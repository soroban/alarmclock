//
//  MasterViewController.h
//  Naptimer
//
//  Created by 大口 尚紀(管理者) on 2014/05/07.
//  Copyright (c) 2014年 大口 尚紀(管理者). All rights reserved.
//

#import <UIKit/UIKit.h>

#import <CoreData/CoreData.h>

@interface MasterViewController : UITableViewController <NSFetchedResultsControllerDelegate>

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
- (IBAction)enterEditMode:(id)sender;
- (IBAction)leaveEditMode:(id)sender;

@end
