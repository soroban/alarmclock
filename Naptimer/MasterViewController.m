//
//  MasterViewController.m
//  Naptimer
//
//  Created by 大口 尚紀(管理者) on 2014/05/07.
//  Copyright (c) 2014年 大口 尚紀(管理者). All rights reserved.
//

#import "MasterViewController.h"
#import "AppDelegate.h"
#import "DetailViewController.h"
#import "MoveViewController.h"
#import "FirstViewController.h"
#import "CompleteViewController.h"

@interface MasterViewController ()
- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath;
@end

@implementation MasterViewController{
    int _buttonIndex;
    float timerCount;
    NSTimer *ontimer;
    AppDelegate *appDelegete;
    int n_hour;
    int n_minute;
    int n_second;
    int hour;
    int minute;
}
- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    UITabBarController *tabBarController = (UITabBarController *)window.rootViewController;
    GADBannerView *bannerView = (GADBannerView*)[tabBarController.view viewWithTag:9999999];
    if(bannerView == nil){
        GADBannerView *bannerView = [[GADBannerView alloc] initWithAdSize:kGADAdSizeLargeBanner];
        bannerView.adSize = kGADAdSizeLargeBanner;
        bannerView.adUnitID = @"ca-app-pub-8256083710740545/8580743719";
        bannerView.tag = 9999999;
        bannerView.rootViewController = tabBarController;
        [tabBarController.view addSubview:bannerView];
        [bannerView loadRequest:[GADRequest request]];
    }
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    bannerView.center = CGPointMake(screenSize.width/2, screenSize.height - tabBarController.tabBar.frame.size.height - (bannerView.frame.size.height / 2)  - 1);
    
    self.tableView.contentInset = UIEdgeInsetsMake(0.0f, 0.0f, 100.0f, 0.0f);
    if ([[UIApplication sharedApplication] respondsToSelector:@selector(registerUserNotificationSettings:)]) {
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeBadge
                                                                                             |UIUserNotificationTypeSound
                                                                                             |UIUserNotificationTypeAlert) categories:nil];
        [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
    } else {
        UIRemoteNotificationType myTypes = UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound;
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:myTypes];
    }
//    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0){
//    UIUserNotificationType types = UIUserNotificationTypeBadge |
//    UIUserNotificationTypeSound | UIUserNotificationTypeAlert;
//    
//    UIUserNotificationSettings *mySettings = [UIUserNotificationSettings
//                                              settingsForTypes:types categories:nil];
//    [[UIApplication sharedApplication] registerUserNotificationSettings:mySettings];
//    }
    appDelegete = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    self.managedObjectContext = [(id)[[UIApplication sharedApplication] delegate] managedObjectContext];
    
    [self timerStart];
    
    if(appDelegete.on_alarm == true){
        MoveViewController *newVC = [self.storyboard instantiateViewControllerWithIdentifier:@"moveView"];
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:newVC];
        [self presentViewController:nav animated:NO completion:nil];
        return;
    }

    //  ナビゲーションバーのタイトルの色を白色に変更する
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectZero];
    titleLabel.font = [UIFont fontWithName:@"Helvetica" size:15];
    titleLabel.text = NSLocalizedString(@"timer",nil);
    titleLabel.textColor = [UIColor grayColor];
    [titleLabel sizeToFit];
    self.navigationItem.titleView = titleLabel;
    
    if([[self.fetchedResultsController fetchedObjects] count] > 0){
        //	ナビゲーションバーの左ボタンを「編集」に変更。
	    //	押された時に呼び出すメソッドもenterEditModeにする。
    	//	テーブルビューを編集モードから離れさせる。
        
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"edit",nil) style:UIBarButtonItemStylePlain target:self action:@selector(enterEditMode:)];
    }
    //	テーブルビューを編集モードから離れさせる。
    [self.tableView endUpdates];
    [self.tableView setEditing:NO animated:YES];
    
    appDelegete = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    //    self.view.backgroundColor = [DetailViewController color:4];
    appDelegete.itemArray = [NSMutableArray array];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    
    NSEntityDescription *my_entity
    = [NSEntityDescription entityForName:@"Myschedule" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:my_entity];
    
    // ソート条件を指定します。
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"createdat" ascending:NO];
    NSArray *my_sortDescriptors = @[sortDescriptor];
    [fetchRequest setSortDescriptors:my_sortDescriptors];
    NSArray *my_result= [self.managedObjectContext executeFetchRequest:fetchRequest error:nil];
    [appDelegete.itemArray removeAllObjects];
    for (int i = 0; i < my_result.count; i++) {
        id obj = [my_result objectAtIndex: i];
        [appDelegete.itemArray addObject:[obj valueForKey:@"minute"]];
    }
}

-(void)viewWillDisappear:(BOOL)animated
{
    if([[self.fetchedResultsController fetchedObjects] count] > 0){
        self.navigationItem.hidesBackButton = NO;
        //	ナビゲーションバーの左ボタンを「編集」に変更。
        //	押された時に呼び出すメソッドもenterEditModeにする。
        //	テーブルビューを編集モードから離れさせる。
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"edit",nil) style:UIBarButtonItemStylePlain target:self action:@selector(enterEditMode:)];
    }
    [self.tableView endUpdates];
    [self.tableView setEditing:NO animated:YES];
}

- (IBAction)mainViewReturnActionForSegue:(UIStoryboardSegue *)segue
{
}

- (void)timerStart {
    ontimer = [NSTimer scheduledTimerWithTimeInterval:1
                                             target:self
                                           selector:@selector(timer)
                                           userInfo:nil
                                            repeats:YES
             ];
}

- (void)timer{
    NSDate *now = [NSDate date];
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSUInteger flags;
    NSDateComponents *comps;
    
    // 年・月・日を取得
//    flags = NSYearCalendarUnit | NSMonthCalendarUnit | NSDayCalendarUnit;
//    comps = [calendar components:flags fromDate:now];
//    
//    NSInteger n_year = comps.year;
//    NSInteger n_month = comps.month;
//    NSInteger n_day = comps.day;
    
    // 時・分・秒を取得
    flags = NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond;
    comps = [calendar components:flags fromDate:now];
    
    n_hour = (int)comps.hour;
    n_minute = (int)comps.minute;
    n_second = (int)comps.second;
    
    // 曜日
    comps = [calendar components:NSCalendarUnitWeekday fromDate:now];
    NSInteger n_weekday = comps.weekday; // 曜日(1が日曜日 7が土曜日)
//    NSLog(@"aaaaaaaa");
    if(n_second == 0){
        //タイマーをセットする
        NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
        
        // 対象エンティティを指定します。
        NSEntityDescription *entity
        = [NSEntityDescription entityForName:@"Alarm" inManagedObjectContext:self.managedObjectContext];
        [fetchRequest setEntity:entity];
        
        // 検索条件を指定します。
        NSPredicate *pred
        = [NSPredicate predicateWithFormat:@"on = YES"];
        [fetchRequest setPredicate:pred];
        // ソート条件を指定します。
        NSSortDescriptor *hourDisc = [[NSSortDescriptor alloc] initWithKey:@"hour" ascending:YES];
        NSSortDescriptor *minuteDisc = [[NSSortDescriptor alloc] initWithKey:@"minute" ascending:YES];
        NSArray *sortDescriptors = [NSArray arrayWithObjects:hourDisc,minuteDisc, nil];
        [fetchRequest setSortDescriptors:sortDescriptors];
        NSArray *result= [self.managedObjectContext executeFetchRequest:fetchRequest error:nil];
        // 上記リクエストを元に件数を取得します。
        for (int i = 0; i < result.count; i++) {
            id obj = [result objectAtIndex: i];
            hour = [[obj valueForKey:@"hour"] intValue];
            minute = [[obj valueForKey:@"minute"] intValue];
            int no_repeat = 0;
            int sun = [[obj valueForKey:@"sun"] intValue];
            int mon = [[obj valueForKey:@"mon"] intValue];
            int tue = [[obj valueForKey:@"tue"] intValue];
            int wed = [[obj valueForKey:@"wed"] intValue];
            int thu = [[obj valueForKey:@"thu"] intValue];
            int fri = [[obj valueForKey:@"fri"] intValue];
            int sat = [[obj valueForKey:@"sat"] intValue];
            
            if(sun == 0 && mon == 0 && tue == 0 && wed == 0 && thu == 0 && fri == 0 && sat == 0){
                no_repeat = 1;
            }
            int db_weekday = 0;
            switch (n_weekday) {
                case 1:
                    if(sun == 1){
                        db_weekday = 1;
                    }
                    break;
                case 2:
                    if(mon == 1){
                        db_weekday = 1;
                    }
                    break;
                case 3:
                    if(tue == 1){
                        db_weekday = 1;
                    }
                    break;
                case 4:
                    if(wed == 1){
                        db_weekday = 1;
                    }
                    break;
                case 5:
                    if(thu == 1){
                        db_weekday = 1;
                    }
                    break;
                case 6:
                    if(fri == 1){
                        db_weekday = 1;
                    }
                    break;
                case 7:
                    if(sat == 1){
                        db_weekday = 1;
                    }
            }
            
            appDelegete = (AppDelegate *)[[UIApplication sharedApplication] delegate];
            if((int)appDelegete.on_alarm == 0){
                if(no_repeat == 1 || db_weekday == 1){
                    if(n_hour == hour && n_minute == minute){
                        if(no_repeat == 1){
                            [obj setValue:[NSNumber numberWithBool:NO] forKey:@"on"];
                        
                            // saveメソッドでデータをDBに保存します。
                            // エラー発生時には、引数に参照渡しで渡しているErrorオブジェクトの中身を表示します。
                            NSError *error = nil;
                            if (![self.managedObjectContext save:&error]) {
                                NSLog(@"error = %@", error);
                            } else {
                                NSLog(@"off alarm.");
                            }
                        }
                    }
                    if(n_hour == hour && n_minute == minute){
                        [ontimer invalidate];
                        [self timerStart];
                        [self goMoveView];
                    }
                }
            }
        }
    }
}

- (void)goMoveView
{
//    [self performSegueWithIdentifier:@"Move" sender:self];
//    MoveViewController *newVC = [self.storyboard instantiateViewControllerWithIdentifier:@"moveView"];
//    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:newVC];
//    [self.navigationController presentViewController:nav animated:NO completion:nil];

//    MoveViewController *newVC = [self.storyboard instantiateViewControllerWithIdentifier:@"moveView"];
//    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:newVC];
//    [[[[UIApplication sharedApplication] delegate] window] setRootViewController:nav];
    
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    UIViewController *rootViewController = window.rootViewController;
    [[[[UIApplication sharedApplication] delegate] window] setRootViewController:rootViewController];
    UITabBarController *tbc = (UITabBarController *)window.rootViewController;
    UINavigationController *navigationController = tbc.viewControllers[0];
    
    NSString* viewIdentifier = @"moveView";
    UIStoryboard* sb = self.storyboard;
    UIViewController* vc = [[UIViewController alloc] init];
    vc = [sb instantiateViewControllerWithIdentifier:viewIdentifier];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:vc];
    [navigationController presentViewController:nav animated:NO completion:nil];

    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView deselectRowAtIndexPath:indexPath animated:YES]; // 選択状態の解除
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    
    return [[self.fetchedResultsController sections] count];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    id <NSFetchedResultsSectionInfo> sectionInfo = [self.fetchedResultsController sections][section];
    return [sectionInfo numberOfObjects];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    [self configureCell:cell atIndexPath:indexPath];
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        NSManagedObjectContext *context = [self.fetchedResultsController managedObjectContext];
        [context deleteObject:[self.fetchedResultsController objectAtIndexPath:indexPath]];
        [tableView endUpdates];
        
        if([[self.fetchedResultsController fetchedObjects] count] <= 1){
            self.navigationItem.leftBarButtonItem = nil;
            [self.tableView endUpdates];
            [self.tableView setEditing:NO animated:YES];
        }
        
        [self.tableView reloadData];
    }
}

- (UITableViewCellEditingStyle)tableView:(UITableView *)tableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath {
    // Detemine if it's in editing mode
    if (self.tableView.editing) {
        return UITableViewCellEditingStyleDelete;
    }
    return UITableViewCellEditingStyleNone;
}

- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    appDelegete = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    appDelegete.itemArray = [NSMutableArray array];
    // The table view should not be re-orderable.
    return NO;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    appDelegete = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    //    [self.tableView endUpdates];
    [self leaveEditMode:(id)sender];
    if ([[segue identifier] isEqualToString:@"showDetail"]) {
        DetailViewController *detailView = [segue destinationViewController];
        detailView.receiveString = [appDelegete.itemArray objectAtIndex:[self.tableView indexPathForSelectedRow].row];
        NSLog(@"%@", detailView.receiveString);
    }
    
}

#pragma mark - Fetched results controller

- (NSFetchedResultsController *)fetchedResultsController
{
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    // Edit the entity name as appropriate.
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Myschedule" inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:entity];
    
    // Set the batch size to a suitable number.
    [fetchRequest setFetchBatchSize:20];
    
    // Edit the sort key as appropriate.
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"createdat" ascending:NO];
    NSArray *sortDescriptors = @[sortDescriptor];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    // Edit the section name key path and cache name if appropriate.
    // nil for section name key path means "no sections".
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:nil cacheName:nil];
    aFetchedResultsController.delegate = self;
    self.fetchedResultsController = aFetchedResultsController;
    
	NSError *error = nil;
	if (![self.fetchedResultsController performFetch:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
	    NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
	    abort();
	}
    
    return _fetchedResultsController;
}

- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo
           atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject
       atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type
      newIndexPath:(NSIndexPath *)newIndexPath
{
    appDelegete = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    UITableView *tableView = self.tableView;
    [appDelegete.itemArray removeAllObjects];
    for (int i = 0; i < [[_fetchedResultsController fetchedObjects] count]; i++) {
        [appDelegete.itemArray addObject:[[[_fetchedResultsController fetchedObjects] valueForKey:@"minute"] objectAtIndex:i]];
    }
    
    switch(type) {
        case NSFetchedResultsChangeInsert:
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            //            if([[self.fetchedResultsController fetchedObjects] count] > 0){
            //                self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"edit",nil) style:UIBarButtonItemStyleBordered target:self action:@selector(enterEditMode:)];
            //            }else{
            //                self.navigationItem.leftBarButtonItem = nil;
            //                self.navigationItem.hidesBackButton = YES;
            //            }
            
            break;
            
        case NSFetchedResultsChangeDelete:{
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            
            //            self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"done",nil) style:UIBarButtonItemStyleBordered target:self action:@selector(leaveEditMode:)];
            break;
        }
            
        case NSFetchedResultsChangeUpdate:
            break;
            
        case NSFetchedResultsChangeMove:
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
            [tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationFade];
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
{
    [self.tableView endUpdates];
}

/*
 // Implementing the above methods to update the table view in response to individual changes may have performance implications if a large number of changes are made simultaneously. If this proves to be an issue, you can instead just implement controllerDidChangeContent: which notifies the delegate that all section and object changes have been processed.
 
 - (void)controllerDidChangeContent:(NSFetchedResultsController *)controller
 {
 // In the simplest, most efficient, case, reload the table view.
 [self.tableView reloadData];
 }
 */

- (void)configureCell:(UITableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath
{
    NSManagedObject *object = [self.fetchedResultsController objectAtIndexPath:indexPath];
    int c_hour = [[object valueForKey:@"minute"] intValue]/60;
    int c_min = [[object valueForKey:@"minute"] intValue]%60;
    if(c_hour == 0){
        cell.textLabel.text = [[NSString stringWithFormat:@"%dmin", c_min] description];
    }else{
        cell.textLabel.text = [[NSString stringWithFormat:@"%dh %dmin",c_hour, c_min] description];
    }
    appDelegete = (AppDelegate *)[[UIApplication sharedApplication] delegate];
    [appDelegete.itemArray addObject:[object valueForKey:@"minute"]];
}

- (IBAction)enterEditMode:(id)sender {
    //	ビゲーションバーの左ボタンを「終了」に変更。押された時に
	//	テーブルビューを編集モード突入にさせる。
    self.navigationItem.hidesBackButton = NO;
    self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"done",nil) style:UIBarButtonItemStylePlain target:self action:@selector(leaveEditMode:)];
	[self.tableView setEditing:YES animated:YES];
	[self.tableView beginUpdates];
    
}

- (IBAction)leaveEditMode:(id)sender {
    // エラー発生時には、引数に参照渡しで渡しているErrorオブジェクトの中身を表示します。
    NSError *error = nil;
    if (![self.managedObjectContext save:&error]) {
        NSLog(@"error = %@", error);
    } else {
        NSLog(@"off alarm.");
    }
    
    if([[self.fetchedResultsController fetchedObjects] count] > 0){
        self.navigationItem.hidesBackButton = NO;
        //	ナビゲーションバーの左ボタンを「編集」に変更。
	    //	押された時に呼び出すメソッドもenterEditModeにする。
    	//	テーブルビューを編集モードから離れさせる。
        self.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"edit",nil) style:UIBarButtonItemStylePlain target:self action:@selector(enterEditMode:)];
    }
    //	テーブルビューを編集モードから離れさせる。
    [self.tableView endUpdates];
    [self.tableView setEditing:NO animated:YES];
}

- (void)viewWillAppear:(BOOL)animated
{
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    UITabBarController *tabBarController = (UITabBarController *)window.rootViewController;
    GADBannerView *bannerView = (GADBannerView*)[tabBarController.view viewWithTag:9999999];
    CGRect rect1 = [[UIScreen mainScreen] bounds];
    
    if(bannerView == nil){
        GADBannerView *bannerView = [[GADBannerView alloc] initWithAdSize:kGADAdSizeLargeBanner];
        bannerView.adSize = kGADAdSizeLargeBanner;
        bannerView.adUnitID = @"ca-app-pub-8256083710740545/8580743719";
        bannerView.tag = 9999999;
        bannerView.rootViewController = tabBarController;
        [tabBarController.view addSubview:bannerView];
        [bannerView loadRequest:[GADRequest request]];
    }
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    bannerView.center = CGPointMake(rect1.size.width/2, screenSize.height - tabBarController.tabBar.frame.size.height - (bannerView.frame.size.height / 2)  - 1);
}

- (void)viewDidAppear:(BOOL)animated
{
}


@end
