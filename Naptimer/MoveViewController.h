//
//  MoveViewController.h
//  Naptimer
//
//  Created by 大口 尚紀(管理者) on 2014/08/07.
//  Copyright (c) 2014年 大口 尚紀(管理者). All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MediaPlayer/MediaPlayer.h>
#include <AVFoundation/AVFoundation.h>

@interface MoveViewController : UIViewController
{
    float initialVolume;
    AVAudioSession *session;
}

- (void)setVolumeNotification; // ボリュームボタ
@end
