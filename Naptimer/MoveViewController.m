//
//  MoveViewController.m
//  Naptimer
//
//  Created by 大口 尚紀(管理者) on 2014/08/07.
//  Copyright (c) 2014年 大口 尚紀(管理者). All rights reserved.
//

#import "MoveViewController.h"
#import "AppDelegate.h"
#import <AudioToolbox/AudioToolbox.h>
#import "CompleteViewController.h"
#import <MediaPlayer/MediaPlayer.h>

#define UIColorHex(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

@interface MoveViewController ()
@end

@implementation MoveViewController{
    AppDelegate *appDelegete;
    float threshold;
    int pre_update_time;
    int not_update_time;
    int pre_not_update_time;
    double no_update_count;
    UIView *chart;
    UILabel *percentLabel;
    UILabel *explainLabel;
    int update_time;
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewWillAppear:(BOOL)animated
{
}

- (void)viewWillDisappear:(BOOL)animated
{
//    UIWindow *window = [UIApplication sharedApplication].keyWindow;
//    UITabBarController *tabBarController = (UITabBarController *)window.rootViewController;
//    GADBannerView *bannerView = (GADBannerView*)[tabBarController.view viewWithTag:9999999];
//    if(bannerView != nil){
//        CGRect bannerFrame = CGRectMake(0.0,
//                                        tabBarController.view.frame.size.height -
//                                        GAD_SIZE_320x50.height - 49,
//                                        GAD_SIZE_320x50.width, GAD_SIZE_320x50.height);
//        bannerView.frame = bannerFrame;
//    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self setVolumeNotification];
    
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center addObserver:self selector:@selector(applicationDidEnterBackground:) name:UIApplicationDidEnterBackgroundNotification object:nil];
    [center addObserver:self selector:@selector(applicationWillEnterForeground:) name:UIApplicationWillEnterForegroundNotification object:nil];
//    UIWindow *window = [UIApplication sharedApplication].keyWindow;
//    UITabBarController *tabBarController = (UITabBarController *)window.rootViewController;
//    
//    GADBannerView *bannerView = [[GADBannerView alloc] initWithAdSize:kGADAdSizeLargeBanner];
//    bannerView.adSize = kGADAdSizeLargeBanner;
//    CGRect rect1 = [[UIScreen mainScreen] bounds];
//    bannerView.center = CGPointMake(rect1.size.width/2, tabBarController.view.frame.size.height - (bannerView.frame.size.height/2));
//    bannerView.adUnitID = @"ca-app-pub-8256083710740545/8580743719";
//    bannerView.tag = 9999999;
//    [self.view addSubview:bannerView];
//    bannerView.rootViewController = tabBarController;
//    [bannerView loadRequest:[GADRequest request]];

    //以下追加
    pre_update_time = [[NSDate date] timeIntervalSince1970];
    not_update_time = [[NSDate date] timeIntervalSince1970];
    pre_not_update_time = [[NSDate date] timeIntervalSince1970];
    no_update_count = 0;

    
    AVAudioSession *session = [AVAudioSession sharedInstance];
    [session setCategory:AVAudioSessionCategoryPlayback error:NULL];
    [session setActive:YES error:NULL];
    
    //ナビゲーションバーのタイトルの色を白色に変更する
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectZero];
    titleLabel.font = [UIFont fontWithName:@"Helvetica" size:15];
    titleLabel.text = NSLocalizedString(@"timer",nil);
    titleLabel.textColor = [UIColor grayColor];
    [titleLabel sizeToFit];
    self.navigationItem.titleView = titleLabel;
    
    appDelegete = [[UIApplication sharedApplication] delegate];
    [self getLight];
    appDelegete.on_alarm = true;
    appDelegete.sub = 0;
    [self stopTimer];
  //  [self updatePieChart];
}
    
- (void)setVolumeNotification {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(volumeChanged:) name:@"AVSystemController_SystemVolumeDidChangeNotification" object:nil];
}
    
- (void)volumeChanged:(NSNotification *)notification{
    //明示的にボリューム変更がされた時のみ
    if ([[[notification userInfo] objectForKey:@"AVSystemController_AudioVolumeChangeReasonNotificationParameter"] isEqualToString:@"ExplicitVolumeChange"]) {
        
        //ここに押下処理を記述
        MPMusicPlayerController *playerController = [MPMusicPlayerController systemMusicPlayer];
        [playerController setValue:@(1.0) forKey:@"volume"];
        [playerController setValue:@(1.0) forKey:@"volumePrivate"];
        
        //一旦NSNotificationCenterからAVSystemController_SystemVolumeDidChangeNotificationを外して、ボリュームを元に戻す
        [[NSNotificationCenter defaultCenter] removeObserver:self name:@"AVSystemController_SystemVolumeDidChangeNotification" object:nil];
        [self performSelector:@selector(setVolumeNotification) withObject:nil afterDelay:0.2];
    }
}
    
- (void)applicationDidEnterBackground:(NSNotification *)notification {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"AVSystemController_SystemVolumeDidChangeNotification" object:nil];
    [session setActive:NO error:nil];
}
    
- (void)applicationWillEnterForeground:(NSNotification *)notification {
    [self setVolumeNotification];
    [session setActive:YES error:nil];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)getLight
{
    [UIApplication sharedApplication].idleTimerDisabled = YES;
    threshold = 0.4;
    
    CGRect rect1 = [[UIScreen mainScreen] bounds];
    // UIImageViewの初期化
    CGRect rect_taisou = CGRectMake(10, rect1.size.height-(rect1.size.height/2.9), 130, 130);
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:rect_taisou];
    
    // 画像の読み込み
    imageView.image = [UIImage imageNamed:@"taisou.png"];
    imageView.center = CGPointMake(rect1.size.width/4, rect1.size.height-(rect1.size.height/4.2));
    // UIImageViewのインスタンスをビューに追加
    [self.view addSubview:imageView];
    
    
    CGRect rect_aruku = CGRectMake(150, rect1.size.height-(rect1.size.height/2.9), 130, 130);
    imageView = [[UIImageView alloc]initWithFrame:rect_aruku];
    
    // 画像の読み込み
    imageView.image = [UIImage imageNamed:@"aruku.png"];
    imageView.center = CGPointMake(rect1.size.width*3/4, rect1.size.height-(rect1.size.height/4.2));
    // UIImageViewのインスタンスをビューに追加
    [self.view addSubview:imageView];
    
    [self startPieChart];
    [self playSound];
        
    // 加速度データの更新間隔を0.1秒ごとに設定
    appDelegete.motionManager = [[CMMotionManager alloc] init];
    // 加速度データの更新間隔を0.1秒ごとに設定
    appDelegete.motionManager.accelerometerUpdateInterval = 0.5;
    
    //    __block double change_date = 0.0;
    // 加速度センサが利用可能かチェック
    if (appDelegete.motionManager.accelerometerAvailable)
    {
        // モーションデータ更新時のハンドラを作成
        void (^handler)(CMDeviceMotion *, NSError *) = ^(CMDeviceMotion *motion, NSError *error)
        {
            // ユーザー加速度の重力方向の大きさを算出
            double magnitude = [self gravityDirectionMagnitudeForMotion:motion];
            if(threshold < fabs(magnitude)){
                [self updatePieChart];
                no_update_count = 0;
            }else{
                pre_not_update_time = not_update_time;
                not_update_time = [[NSDate date] timeIntervalSince1970];
            }
            
            if((not_update_time-pre_not_update_time) == 0){
                no_update_count++;
            }
            if(no_update_count > 100){
                if(appDelegete.player.volume <= 1.0){
                    appDelegete.player.volume = appDelegete.player.volume + 0.1;
                }
                no_update_count = 0;
            }
            
            //停止
            //if(appDelegete.percent > 0.01){
            if(appDelegete.percent > 1.01){
                [appDelegete.motionManager stopDeviceMotionUpdates];
                [self stopTimer];
            }
        };
        
        // モーションデータの測定を開始
        NSOperationQueue *queue = [NSOperationQueue currentQueue];
        [appDelegete.motionManager startDeviceMotionUpdatesToQueue:queue withHandler:handler];
    }
}

- (void)startPieChart
{
    CGRect rect1 = [[UIScreen mainScreen] bounds];
    //アニメーションの対象となるコンテキスト
    CGContextRef context = UIGraphicsGetCurrentContext();
    [UIView beginAnimations:nil context:context];
    //アニメーションを実行する時間
    [UIView setAnimationDuration:1.0];
    //アニメーションイベントを受け取るview
    [UIView setAnimationDelegate:self.view];
    
    chart = [[UIView alloc] initWithFrame:CGRectMake((rect1.size.width/2) - 100, 100, 200, 200)];
    chart.center = CGPointMake(rect1.size.width/2, chart.center.y);
    chart.backgroundColor = [UIColor grayColor];
    chart.layer.cornerRadius = 100;
    chart.alpha = 0.0;
    
    [self.view addSubview:chart];
    UIBezierPath *path = path = [UIBezierPath bezierPath];
    [path moveToPoint:CGPointMake(chart.center.x, 100)];
    [path addArcWithCenter:CGPointMake(chart.center.x, 100) radius:100 startAngle:-0.5*M_PI endAngle:-0.5*M_PI + ((2*M_PI)*0) clockwise:YES];
    CAShapeLayer *sl;
    sl = [[CAShapeLayer alloc] init];
    sl.fillColor = [self color:1].CGColor;
    sl.path = path.CGPath;
    [chart.layer addSublayer:sl];
    [chart setAlpha:1.0];
    [UIView commitAnimations];
    appDelegete.percent = 0;
    percentLabel = [[UILabel alloc] init];
    percentLabel.frame = CGRectMake(102, chart.center.y-22, 120, 50);
    percentLabel.font = [UIFont fontWithName:@"Helvetica" size:23];
    percentLabel.textColor = [UIColor grayColor];
    percentLabel.textAlignment = NSTextAlignmentCenter;
    percentLabel.text = @"0.00%";
    percentLabel.center = CGPointMake(rect1.size.width/2 + 2, percentLabel.center.y);
    [self.view addSubview:percentLabel];
    
    UIView *mask = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 120, 120)];
    mask.layer.cornerRadius = 60;
    mask.center = CGPointMake(100, 100);
    mask.backgroundColor = [UIColor whiteColor];
    [chart addSubview:mask];
    
    UILabel *meterLabel = [[UILabel alloc] init];
    meterLabel.frame = CGRectMake(102, 290, 120, 50);
    meterLabel.font = [UIFont fontWithName:@"Helvetica" size:20];
    meterLabel.textColor = [UIColor grayColor];
    meterLabel.textAlignment = NSTextAlignmentCenter;
    meterLabel.text = @"move meter";
    meterLabel.center = CGPointMake(rect1.size.width/2 + 2, meterLabel.center.y);
    [self.view addSubview:meterLabel];
    
    explainLabel = [[UILabel alloc] init];
    explainLabel.frame = CGRectMake(rect1.size.width*0.2/2, rect1.size.height-(rect1.size.height/1.22), rect1.size.width*0.8, 500);
    explainLabel.font = [UIFont fontWithName:@"Helvetica" size:14];
    explainLabel.textColor = [UIColor grayColor];
    explainLabel.textAlignment = NSTextAlignmentLeft;
    explainLabel.numberOfLines = 0;
    explainLabel.text = NSLocalizedString(@"explain",nil);
    [self.view addSubview:explainLabel];
    
}

- (void)updatePieChart
{
    pre_update_time = [[NSDate date] timeIntervalSince1970];
    not_update_time = [[NSDate date] timeIntervalSince1970];
    pre_not_update_time = [[NSDate date] timeIntervalSince1970];
    no_update_count = 0;

    appDelegete.percent = appDelegete.percent +0.00009;
    appDelegete.sub = appDelegete.sub + 0.00009;
//    appDelegete.percent = appDelegete.percent + 0.1;
//        appDelegete.sub = appDelegete.sub + 0.1;
//        appDelegete.percent =1.30;
    
    NSString *text = percentLabel.text;
    if([text isEqualToString:[NSString stringWithFormat:@"%04.1f%%",appDelegete.percent*100]]) {
        
    }else{
        percentLabel.text = nil;
        percentLabel.text = [NSString stringWithFormat:@"%04.1f%%",appDelegete.percent*100];
        
        UIBezierPath *path = [UIBezierPath bezierPath];
        [path moveToPoint:CGPointMake(100, 100)];
        CAShapeLayer *sl = [[CAShapeLayer alloc] init];
        
        if(appDelegete.percent <= 0.5){
            [path addArcWithCenter:CGPointMake(100, 100) radius:100 startAngle:-0.5*M_PI endAngle:-0.5*M_PI + ((2*M_PI)*(appDelegete.percent+0.01)) clockwise:YES];
        }else{
            [path addArcWithCenter:CGPointMake(100, 100) radius:100 startAngle:0.5*M_PI endAngle:0.5*M_PI + ((2*M_PI) * ((appDelegete.percent+0.01)-0.5)) clockwise:YES];
        }
        sl.fillColor = [self color:1].CGColor;
        sl.path = path.CGPath;
        [chart.layer insertSublayer:sl atIndex:0];
        appDelegete.sub =0;
        
        update_time = [[NSDate date] timeIntervalSince1970];
        if((update_time - pre_update_time) <= 1){
            if(appDelegete.player.volume >= 0.0){
                appDelegete.player.volume = appDelegete.player.volume - 0.1;
            }
        }
        pre_update_time = update_time;
    }
    
    if(appDelegete.percent >= 1.00){
        [self stopTimer];
    }
}

- (double)gravityDirectionMagnitudeForMotion:(CMDeviceMotion *)motion
{
    // ユーザー加速度の測定値を取得
    CMAcceleration user = motion.userAcceleration;
    // 重力加速度の測定値を取得
    //    CMAcceleration gravity = motion.gravity;
    
    // ユーザー加速度の大きさを算出
    double magnitude = sqrt(pow(user.x, 2) + pow(user.y, 2) + pow(user.z, 2));
    
    // ユーザー加速度のベクトルと重力加速度のベクトルのなす角θのcosθを算出
    //    double cosT = (user.x * gravity.x + user.y * gravity.y + user.z * gravity.z) /
    //    sqrt((pow(user.x, 2) + pow(user.y, 2) + pow(user.z, 2)) *
    //         (pow(gravity.x, 2) + pow(gravity.y, 2) + pow(gravity.z, 2)));
    
    // ユーザー加速度の大きさにcosθを乗算してユーザー加速度の重力方向における大きさを算出し、小数点第3位で丸める
    //    double gravityDirectionMagnitude = round(magnitude * cosT * 100) / 100;
    double gravityDirectionMagnitude = round(magnitude * 100) / 100;
    return gravityDirectionMagnitude;
}

- (void)playSound{
    MPMusicPlayerController *playerController = [MPMusicPlayerController systemMusicPlayer];
    [playerController setValue:@(1.0) forKey:@"volume"];
    [playerController setValue:@(1.0) forKey:@"volumePrivate"];
    
    NSString *soundPath = [[NSBundle mainBundle] pathForResource:@"normal_alarm" ofType:@"m4a"];
    NSURL *url = [NSURL fileURLWithPath:soundPath];
    appDelegete.player = [[AVAudioPlayer alloc] initWithContentsOfURL:url error:NULL];
    [appDelegete.player setNumberOfLoops:0];
    appDelegete.player.volume = 1.0;
    appDelegete.player.numberOfLoops = -1;
    appDelegete.player.delegate = (id)self;
    [appDelegete.player prepareToPlay];
    [appDelegete.player setVolume:1.0]; 
    [appDelegete.player play];
}

- (void)stopTimer{
    appDelegete = [[UIApplication sharedApplication] delegate];
    appDelegete.on_alarm = false;
    [appDelegete.player stop];
//    [self performSegueWithIdentifier:@"goComplete" sender:self];
    CompleteViewController *newVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CompleteView"];
    UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:newVC];
    [self.navigationController presentViewController:nav animated:NO completion:nil];
    
}

- (UIColor*)color:(int)i
{
    switch (i) {
        case 0:
            return UIColorHex(0xF24495);
        case 1:
            return UIColorHex(0x04BFBF);
        case 2:
            return UIColorHex(0xB2F252);
        case 3:
            return UIColorHex(0xF2CB05);
        case 4:
            return UIColorHex(0xE9F2DF);
        default:
            break;
    }
    return nil;
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}

@end
