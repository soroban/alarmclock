//
//  SettingViewController.m
//  AlarmClock
//
//  Created by 大口 尚紀 on 2018/01/06.
//  Copyright © 2018年 大口 尚紀(管理者). All rights reserved.
//

#import "SettingViewController.h"
#import "MasterViewController.h"
#import "AppDelegate.h"

@interface SettingViewController ()
@property (weak, nonatomic) IBOutlet UITextView *label1;
@property (weak, nonatomic) IBOutlet UITextView *label2;
@property (weak, nonatomic) IBOutlet UITextView *label3;

@end

@implementation SettingViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectZero];
    titleLabel.font = [UIFont fontWithName:@"Helvetica" size:15];
    titleLabel.text = NSLocalizedString(@"set0",nil);
    titleLabel.textColor = [UIColor grayColor];
    [titleLabel sizeToFit];
    self.navigationItem.titleView = titleLabel;
    
    [self.label1 setFont:[UIFont systemFontOfSize:22]];
    [self.label2 setFont:[UIFont systemFontOfSize:22]];
    [self.label3 setFont:[UIFont systemFontOfSize:22]];
    
    NSString *label1No = NSLocalizedString(@"label1No",nil);
    NSString *label1Red = NSLocalizedString(@"label1red",nil);
    NSString *label1 = NSLocalizedString(@"label1",nil);

    NSString *label2No = NSLocalizedString(@"label2No",nil);
    NSString *label2Red = NSLocalizedString(@"label2red",nil);
    NSString *label2 = NSLocalizedString(@"label2",nil);

    NSString *label3No = NSLocalizedString(@"label3No",nil);
    NSString *label3Red = NSLocalizedString(@"label3red",nil);
    NSString *label3 = NSLocalizedString(@"label3",nil);

    NSMutableAttributedString *attrStr1;
    NSMutableAttributedString *labelJoin1;
    NSMutableAttributedString *labelNormal1;
    NSMutableAttributedString *attrStr2;
    NSMutableAttributedString *labelJoin2;
    NSMutableAttributedString *labelNormal2;
    NSMutableAttributedString *attrStr3;
    NSMutableAttributedString *labelJoin3;
    NSMutableAttributedString *labelNormal3;
    attrStr1 = [[NSMutableAttributedString alloc] initWithString:label1Red];    // 文字色
    [attrStr1 addAttribute:NSForegroundColorAttributeName
                    value:[UIColor redColor]
                range:NSMakeRange(0, [attrStr1 length])];
    attrStr2 = [[NSMutableAttributedString alloc] initWithString:label2Red];    // 文字色
    [attrStr2 addAttribute:NSForegroundColorAttributeName
                     value:[UIColor redColor]
                     range:NSMakeRange(0, [attrStr2 length])];
    
    attrStr3 = [[NSMutableAttributedString alloc] initWithString:label3Red];    // 文字色
    [attrStr3 addAttribute:NSForegroundColorAttributeName
                     value:[UIColor redColor]
                     range:NSMakeRange(0, [attrStr3 length])];
    
    labelJoin1 = [[NSMutableAttributedString alloc] initWithString:label1No];
    labelNormal1 = [[NSMutableAttributedString alloc] initWithString:label1];
    [labelJoin1 appendAttributedString:attrStr1];
    [labelJoin1 appendAttributedString:labelNormal1];
    [labelJoin1 addAttribute:NSFontAttributeName
                       value:[UIFont fontWithName:nil size:19.]
                       range:NSMakeRange(0, [labelJoin1 length])];
    [self.label1 setAttributedText:labelJoin1];
    
    labelJoin2 = [[NSMutableAttributedString alloc] initWithString:label2No];
    labelNormal2 = [[NSMutableAttributedString alloc] initWithString:label2];
    [labelJoin2 appendAttributedString:attrStr2];
    [labelJoin2 appendAttributedString:labelNormal2];
    [labelJoin2 addAttribute:NSFontAttributeName
                       value:[UIFont fontWithName:nil size:19.]
                       range:NSMakeRange(0, [labelJoin2 length])];
    [self.label2 setAttributedText:labelJoin2];
    
    labelJoin3 = [[NSMutableAttributedString alloc] initWithString:label3No];
    labelNormal3 = [[NSMutableAttributedString alloc] initWithString:label3];
    [labelJoin3 appendAttributedString:attrStr3];
    [labelJoin3 appendAttributedString:labelNormal3];
    [labelJoin3 addAttribute:NSFontAttributeName
                       value:[UIFont fontWithName:nil size:19.]
                       range:NSMakeRange(0, [labelJoin3 length])];
    [self.label3 setAttributedText:labelJoin3];

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated
{
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    UITabBarController *tabBarController = (UITabBarController *)window.rootViewController;
    GADBannerView *bannerView = (GADBannerView*)[tabBarController.view viewWithTag:9999999];
    CGRect bannerFrame = bannerView.frame;
    bannerFrame.origin.y = self.view.bounds.size.height - (0 * bannerFrame.size.height);
    bannerView.frame = bannerFrame;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
