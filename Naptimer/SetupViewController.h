//
//  SetupViewController.h
//  Naptimer
//
//  Created by 大口 尚紀(管理者) on 2014/05/09.
//  Copyright (c) 2014年 大口 尚紀(管理者). All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface SetupViewController : UIViewController
<UIPickerViewDelegate, NSFetchedResultsControllerDelegate>  // PickerViewのデータやタップ時のイベントのため
{
    NSMutableArray *minutes;    // 分
    NSMutableArray *hours;    //　時間
}
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (weak, nonatomic) IBOutlet UINavigationBar *registNav;

@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;
- (IBAction)save:(id)sender;
@end
