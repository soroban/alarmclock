//
//  SetupViewController.m
//  Naptimer
//
//  Created by 大口 尚紀(管理者) on 2014/05/09.
//  Copyright (c) 2014年 大口 尚紀(管理者). All rights reserved.
//

#import "SetupViewController.h"
#import "CompleteViewController.h"
#import "AppDelegate.h"

@interface SetupViewController ()

@end

@implementation SetupViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    //  ナビゲーションバーのタイトルの色を白色に変更する
    UILabel *titleLabel = [[UILabel alloc]initWithFrame:CGRectZero];
    titleLabel.font = [UIFont fontWithName:@"Helvetica" size:15];
    titleLabel.text = NSLocalizedString(@"addtimer",nil);
    titleLabel.textColor = [UIColor grayColor];
    [titleLabel sizeToFit];
    self.navigationItem.titleView = titleLabel;
    
    id appDelegate = (id)[[UIApplication sharedApplication] delegate];
    self.managedObjectContext = [appDelegate managedObjectContext];
    
    // 現在の日付を取得
    
    // 現在の日付(NSDate)から年と月をintで取得
    int defMinute= 10;
    
    // ラベルに表示
    self.dateLabel.text = [NSString stringWithFormat:@"%d min", defMinute];
    self.dateLabel.font = [UIFont fontWithName:@"Helvetica" size:38];
    self.dateLabel.textColor = [UIColor grayColor];
    
    // Pickerviewにデリゲートを設定
    self.pickerView.delegate = self;
    
    minutes = [[NSMutableArray alloc] initWithCapacity:60];
    for (int i = 0; i <= 59; i++) {
        [minutes addObject:[NSString stringWithFormat:@"%d", i]];
    }
    
    hours =  [[NSMutableArray alloc] initWithCapacity:24];
    for (int j = 0; j < 24; j++) {
        [hours addObject:[NSString stringWithFormat:@"%d", j]];
    }
    
    
    // -------------------------------------------
    // PickerViewに初期の選択値として、現在の日付を設定
    // -------------------------------------------
    // PickerViewの列数を計算する
    // PickerViewの初期の選択値を設定
    // componentが行番号、selectRowが列番号
    [self.pickerView selectRow:defMinute inComponent:1 animated:YES];  // 分を選択
    [self.pickerView selectRow:0 inComponent:0 animated:YES];  // 時を選択
	// Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

# pragma mark UIPIkerView's Delegate
// 列(component)の数を返す
- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    // 列を指定
    return 2;
}

// 列(component)に対する、行(row)の数を返す
- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    switch (component) {
        case 1:
            return [minutes count];   // 分のデータ数を返す
        case 0:
            return [hours count];   // 時のデータ数を返す
    }
    return 0;
}

// 列(component)と行(row)に対応する文字列を返す
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    switch (component) {
        case 1:
            return [minutes objectAtIndex:row];   // 分のデータの列に対応した文字列を返す
        case 0:
            return [hours objectAtIndex:row];   // 年のデータの列に対応した文字列を返す
    }
    return nil;
}


// PickerViewの操作が行われたときに呼ばれる
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row  inComponent:(NSInteger)component {
    
    // PickerViewの選択されている年と月の列番号を取得
    int rowOfMinute  = (int)[pickerView selectedRowInComponent:1];
    int rowOfHour  = (int)[pickerView selectedRowInComponent:0];
    
    if(rowOfMinute == 0 && rowOfHour == 0){
        self.navigationItem.rightBarButtonItem = nil;
    }else{
        if(self.navigationItem.rightBarButtonItem == nil){
            self.navigationItem.hidesBackButton = NO;
            self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:NSLocalizedString(@"save",nil) style:UIBarButtonItemStyleBordered target:self action:@selector(save:)];
        }
    }
    if(rowOfHour != 0){
        self.dateLabel.text = [NSString stringWithFormat:@"%@ h %@ min",
                               [hours objectAtIndex:rowOfHour], [minutes objectAtIndex:rowOfMinute]];
    }else{
        self.dateLabel.text = [NSString stringWithFormat:@"%@ min", [minutes objectAtIndex:rowOfMinute]];
    }
    self.dateLabel.font = [UIFont fontWithName:@"Helvetica" size:38];
    self.dateLabel.textColor = [UIColor grayColor];
}

- (NSString *)pickerView:(UIPickerView *)pickerView getNowVlue:(NSInteger)row  inComponent:(NSInteger)component {
    
    // PickerViewの選択されている年と月の列番号を取得
    int rowOfMinute  = (int)[pickerView selectedRowInComponent:0]; // 年を取得
    int rowOfHour  = (int)[pickerView selectedRowInComponent:1]; // 年を取得
    
    switch (component){
        case 1:
            return [minutes objectAtIndex:rowOfMinute];
        case 0:
            return [hours objectAtIndex:rowOfHour];
    }
    return nil;
}

- (IBAction)save:(id)sender {
    [self insertNewObject:(id)sender];
    [self performSegueWithIdentifier:@"MainViewUnwindSegue" sender:self];
}

- (void)insertNewObject:(id)sender
{
    int rowOfMinute  = (int)[self.pickerView selectedRowInComponent:1];
    int selectedMinute = [[minutes objectAtIndex:rowOfMinute] intValue];
    
    int rowOfHour  = (int)[self.pickerView selectedRowInComponent:0];
    int selectedHour = [[minutes objectAtIndex:rowOfHour] intValue];
    //idの最大値を取得
    
    NSManagedObjectContext *context = nil;
    id delegate = [[UIApplication sharedApplication] delegate];
    if ([delegate performSelector:@selector(managedObjectContext)]) {
        context = [delegate managedObjectContext];
    }
    NSManagedObject *newManagedObject = [NSEntityDescription insertNewObjectForEntityForName:@"Myschedule"  inManagedObjectContext:context];
    
    // If appropriate, configure the new managed object.
    // Normally you should use accessor methods, but using KVC here avoids the need to add a custom class to the template.
    [newManagedObject setValue:[NSDate date] forKey:@"createdat"];
    [newManagedObject setValue:[NSNumber numberWithInt:selectedMinute + (selectedHour*60)] forKey:@"minute"];
    
    // Save the context.
    NSError *error = nil;
    if (![context save:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        abort();
    }
}

/**
 * 行のサイズを変更
 */
- (CGFloat)pickerView:(UIPickerView *)pickerView
    widthForComponent:(NSInteger)component
{
    switch (component) {
        case 0: // 1列目
            return 100.0;
            break;
            
        case 1: // 2列目
            return 100.0;
            break;
            
        default:
            return 0;
            break;
    }
}

- (UIView *)pickerView:(UIPickerView *)pickerView
            viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view {
    
    UILabel *label = (id)view;
    
    if (!label) {
        label= [[UILabel alloc] initWithFrame:CGRectMake(0.0f, 0.0f, [pickerView rowSizeForComponent:component].width, 216.0)];
    }
    label.text = minutes[row];
    label.font = [UIFont fontWithName:@"Helvetica" size:32];
    label.textColor = [UIColor grayColor];
    label.textAlignment = NSTextAlignmentCenter;
    
    
    return label;
}

- (void)viewWillAppear:(BOOL)animated
{
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    UITabBarController *tabBarController = (UITabBarController *)window.rootViewController;
    GADBannerView *bannerView = (GADBannerView*)[tabBarController.view viewWithTag:9999999];
    if(bannerView != nil){
        CGRect bannerFrame = bannerView.frame;
        bannerFrame.origin.y = self.view.bounds.size.height - (1 * bannerFrame.size.height);
        bannerView.frame = bannerFrame;
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    UITabBarController *tabBarController = (UITabBarController *)window.rootViewController;
    GADBannerView *bannerView = (GADBannerView*)[tabBarController.view viewWithTag:9999999];
    if(bannerView == nil){
        GADBannerView *bannerView = [[GADBannerView alloc] initWithAdSize:kGADAdSizeLargeBanner];
        bannerView.adSize = kGADAdSizeLargeBanner;
        bannerView.adUnitID = @"ca-app-pub-8256083710740545/8580743719";
        bannerView.tag = 9999999;
        bannerView.rootViewController = tabBarController;
        [tabBarController.view addSubview:bannerView];
        [bannerView loadRequest:[GADRequest request]];
    }
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    bannerView.center = CGPointMake(screenSize.width/2, screenSize.height - tabBarController.tabBar.frame.size.height - (bannerView.frame.size.height / 2)  - 1);
}

- (void)viewDidAppear:(BOOL)animated
{
}
@end
