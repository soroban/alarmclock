//
//  WeekendViewController.h
//  Naptimer
//
//  Created by 大口 尚紀(管理者) on 2014/07/16.
//  Copyright (c) 2014年 大口 尚紀(管理者). All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol WeekendViewControllerDelegate;
@interface WeekendViewController : UIViewController
@property (weak, nonatomic) IBOutlet UITableView *week;
@property (nonatomic, strong) NSArray *dataSourceweek;
@property (nonatomic, assign) id<WeekendViewControllerDelegate> delegate;
@property NSMutableArray *week_arr;
@end


@protocol WeekendViewControllerDelegate <NSObject>
//protocolで定義するメソッド
-(void)weekendViewController:(WeekendViewController *)weekendViewController didClose:(NSMutableArray *)week_arr;
@end