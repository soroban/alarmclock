//
//  WeekendViewController.m
//  Naptimer
//
//  Created by 大口 尚紀(管理者) on 2014/07/16.
//  Copyright (c) 2014年 大口 尚紀(管理者). All rights reserved.
//

#import "WeekendViewController.h"
#import "AlarmsetupViewController.h"
#import "AppDelegate.h"

@interface WeekendViewController ()<UITableViewDelegate, UITableViewDataSource>
@end

@implementation WeekendViewController{
}
@synthesize delegate;
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.week.delegate = self;
    self.week.dataSource = self;
    // YESで複数選択・NOで単一選択
    self.week.allowsMultipleSelection = YES;
    self.dataSourceweek = @[ NSLocalizedString(@"e_sun", nil),  NSLocalizedString(@"e_mon",nil),NSLocalizedString(@"e_tue", nil), NSLocalizedString(@"e_wed", nil),NSLocalizedString(@"e_thu",  nil), NSLocalizedString(@"e_fri", nil), NSLocalizedString(@"e_sat",nil)];

    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/**
 テーブルに表示するデータ件数を返します。（必須）
 
 @return NSInteger : データ件数
 */
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSInteger dataCount;
    
    // テーブルに表示するデータ件数を返す
    dataCount = self.dataSourceweek.count;
    return dataCount;
}

/**
 テーブルに表示するセクション（区切り）の件数を返します。（オプション）
 
 @return NSInteger : セクションの数
 */
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

/**
 テーブルに表示するセルを返します。（必須）
 
 @return UITableViewCell : テーブルセル
 */
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    // 再利用できるセルがあれば再利用する
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (!cell) {
        // 再利用できない場合は新規で作成
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault
                                      reuseIdentifier:CellIdentifier];
    }
    
//    cell.textLabel.font = [UIFont fontWithName:@"Helvetica" size:25];
//    cell.detailTextLabel.font = [UIFont systemFontOfSize:25];
    cell.textLabel.text = self.dataSourceweek[indexPath.row];
    cell.detailTextLabel.textColor = [UIColor grayColor];
    if([self.week_arr objectAtIndex:indexPath.row] == [NSNumber numberWithBool:YES]){
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
    }
    return cell;
}

// Cell が選択された時
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    // 選択されたセルを取得
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];

    // セルのアクセサリにチェックマークを指定
    if (cell.accessoryType == UITableViewCellAccessoryNone) {
        //選択されたセルにチェックを入れる
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        [self.week_arr replaceObjectAtIndex:indexPath.row withObject:[NSNumber numberWithBool:YES]];
    }else{
        cell.accessoryType = UITableViewCellAccessoryNone;
        [self.week_arr replaceObjectAtIndex:indexPath.row withObject:[NSNumber numberWithBool:NO]];
    }
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}
- (void)viewDidAppear:(BOOL)animated
{
}

- (void)viewWillAppear:(BOOL)animated
{
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    UITabBarController *tabBarController = (UITabBarController *)window.rootViewController;
    GADBannerView *bannerView = (GADBannerView*)[tabBarController.view viewWithTag:9999999];
    if(bannerView != nil){
        CGRect bannerFrame = bannerView.frame;
        bannerFrame.origin.y = self.view.bounds.size.height - (1 * bannerFrame.size.height);
        bannerView.frame = bannerFrame;
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [delegate weekendViewController:self didClose:self.week_arr];
    UIWindow *window = [UIApplication sharedApplication].keyWindow;
    UITabBarController *tabBarController = (UITabBarController *)window.rootViewController;
    GADBannerView *bannerView = (GADBannerView*)[tabBarController.view viewWithTag:9999999];
    if(bannerView != nil){
        CGRect bannerFrame = bannerView.frame;
        bannerFrame.origin.y = tabBarController.view.frame.size.height - bannerView.frame.size.height - 49;
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone) {
            CGSize screenSize = [[UIScreen mainScreen] bounds].size;
            
            if (@available(iOS 11.0, *)) {
                UIWindow *window = UIApplication.sharedApplication.keyWindow;
                CGFloat bottomPadding = window.safeAreaInsets.bottom;
                bannerFrame.origin.y = tabBarController.view.frame.size.height - bannerView.frame.size.height - bottomPadding - 1;
            }
        }
        bannerView.frame = bannerFrame;
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
