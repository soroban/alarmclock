//
//  main.m
//  Naptimer
//
//  Created by 大口 尚紀(管理者) on 2014/05/07.
//  Copyright (c) 2014年 大口 尚紀(管理者). All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
